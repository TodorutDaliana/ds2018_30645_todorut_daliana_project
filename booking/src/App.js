import Registration from "./loginSignin/Registration";
import Home from "./loginSignin/Homee"
import {BrowserRouter as Router, Route} from 'react-router-dom';
import React, {Component} from "react";
import Regg from "./loginSignin/Registration";
import Clientpage from "./client/Clientpage";
import ViewHotels from "./client/ViewHotels"
import ViewOffers from "./client/ViewOffers"
import ViewAllOffers from "./client/AllOffers"
import AllReservations from "./client/AllReservations";
import Offersa from "./admin/Offers";
import Hotels from "./admin/Hotels";
import Users from "./admin/Users";
import AdminHome from "./admin/AdminHome";

class App extends Component {

    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={Home}/>
                    <Route path="/registration" component={Regg}/>
                    <Route path="/clientpage" component={Clientpage}/>
                    <Route path="/viewhotels" component={ViewHotels}/>
                    <Route path="/viewoffers" component={ViewOffers}/>
                    <Route path="/viewalloffers" component={ViewAllOffers}/>
                    <Route path={'/viewallreservations'} component={AllReservations}/>
                    <Route path={'/offers'} component={Offersa}/>
                    <Route path={'/hotels'} component={Hotels}/>
                    <Route path={'/users'} component={Users}/>
                    <Route path={'/adminpage'} component={AdminHome}/>
                </div>
            </Router>
        )
    }
}

export default App;



