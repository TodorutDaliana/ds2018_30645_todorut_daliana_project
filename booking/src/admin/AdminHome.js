import React,{Component} from 'react';
//import "font-awesome/css/font-awesome.min.css";
//import "bootstrap-css-only/css/bootstrap.min.css";
//import "mdbreact/dist/css/mdb.css";
import {MDBIcon, Button,MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem,FormInline, MDBNavLink, MDBContainer, MDBMask, MDBView,Fa,MDBBtn } from 'mdbreact';
import {CardText} from "../loginSignin/Homee";

class Clientpage extends Component{
    constructor(props){
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false
        }

    }

    onClick =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }


    render() {
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null || localStorage.getItem('typeUser')!== 'admin') {
            this.props.history.push("/")
            return null
        } else {
            return (

                <div>
                    <header>
                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Navbar</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClick}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/adminpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/offers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem >
                                        <MDBNavLink to="/hotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem >
                                        <MDBNavLink to="/users">Users</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon icon="sign-out" /> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                            </MDBCollapse>
                        </MDBNavbar>

                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">
                                <h2>Welcome</h2>
                                <h5>{localStorage.getItem('name')}</h5>
                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-5">
                            <p align="justify">Booking.com is a global technology leader connecting travelers with the
                                widest
                                choice of incredible places to stay. With a mission to empower people to experience
                                the world, Booking.com invests in the digital technology that helps take the
                                friction out of travel. Booking.com connects travelers with the world’s largest
                                selection of incredible places to stay, including everything from apartments,
                                vacation homes, and family-run B&Bs to 5-star luxury resorts, tree houses and even
                                igloos. The Booking.com website and mobile apps are available in 43 languages, offer
                                over 1.5 million properties and cover more than 121,000 destinations in 229
                                countries and territories worldwide</p>
                        </MDBContainer>
                    </main>

                </div>

            )
        }
    }
}


export default Clientpage;