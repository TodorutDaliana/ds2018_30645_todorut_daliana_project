import React from "react";
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";


const HotelForm = (props) => {
    return (

        <div>

            <MDBCard>
                <MDBCardBody>
                    <MDBCardHeader className="form-header aqua-gradient rounded">
                        <h3 >
                            Update hotel:
                        </h3>
                    </MDBCardHeader>
                    <form>
                        <div className="grey-text">
                            <MDBInput
                                value={props.hotel.city}
                                type="name"
                                onChange ={props.getCity}
                            >City</MDBInput>
                            <MDBInput
                                type="name"
                                value={props.hotel.country}
                                onChange ={props.getCountry}
                            > Country</MDBInput>
                            <MDBInput
                                type="name"
                                value={props.hotel.information}
                                onChange ={props.getInformation}
                            > Information</MDBInput>
                            <MDBInput
                                type="name"
                                value={props.hotel.name}
                                onChange ={props.getName}
                            > Name</MDBInput>
                            <MDBInput
                                type="url"
                                value={props.hotel.image}
                                onChange ={props.getImage}
                            >Image</MDBInput>
                            <MDBInput
                                type="number"
                                value={props.hotel.numberDoubleRooms}
                                onChange ={props.getNumberDoubleRoom}
                                >Number double rooms</MDBInput>
                            <MDBInput
                                type="number"
                                value={props.hotel.numberTripleRooms}
                                onChange ={props.getNumberTripleRooms}
                            >Number triple rooms</MDBInput>
                            <MDBInput
                                type="number"
                                value={props.hotel.numberStars}
                                onChange ={props.getNumberStars}
                            >Number stars</MDBInput>
                        </div>
                        <MDBBtn gradient="aqua"
                                rounded
                                size="sm"
                                className="mr-auto"
                                onClick={props.update}><MDBIcon icon="pencil" /></MDBBtn>

                    </form>
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

HotelForm.defaultProps={
    hotel:{}
}
export default HotelForm;