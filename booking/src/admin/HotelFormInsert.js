import React from "react";
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";


const HotelFormInsert = (props) => {
    return (

        <div>

            <MDBCard>
                <MDBCardBody>
                    <MDBCardHeader className="form-header aqua-gradient rounded">
                        <h3 >
                            Insert hotel:
                        </h3>
                    </MDBCardHeader>
                    <form>
                        <div className="grey-text">
                            <MDBInput
                                type="name"
                                onChange ={props.getCity}
                            >City</MDBInput>
                            <MDBInput
                                type="name"
                                onChange ={props.getCountry}
                            > Country</MDBInput>
                            <MDBInput
                            type="name"
                            onChange ={props.getInformation}
                        > Information</MDBInput>
                            <MDBInput
                                type="name"
                                onChange ={props.getName}
                            > Name</MDBInput>
                            <MDBInput
                                type="url"
                                onChange ={props.getImage}
                            >Image</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getNumberDoubleRoom}
                                >Number double rooms</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getNumberTripleRooms}
                            >Number triple rooms</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getNumberStars}
                            >Number stars</MDBInput>
                        </div>
                        <MDBBtn gradient="aqua"
                                rounded
                                size="sm"
                                className="mr-auto"
                                onClick={props.insert}><MDBIcon icon="plus" /></MDBBtn>

                    </form>
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

HotelFormInsert.defaultProps={
    hotel:{}
}
export default HotelFormInsert;