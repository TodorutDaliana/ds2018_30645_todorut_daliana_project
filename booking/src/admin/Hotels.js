import React, {Component} from 'react';
import {
    Modal,
    ModalBody,
    ModalHeader,
    Card,
    CardHeader,
    CardBody,
    TableEditable,
    MDBTableHead,
    MDBTableBody,
    MDBTable,
    MDBBtn,
    MDBIcon,
    MDBCollapse, MDBContainer, MDBMask,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavbarToggler,
    MDBNavItem,
    MDBNavLink,
    MDBView
} from "mdbreact";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Alert} from "react-bootstrap";
import HotelFormInsert from "./HotelFormInsert";
import HotelForm from "./HotelForm";
import '../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css'

class Hotels extends Component {

    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
            errorListHotel: null,
            hotels: [],
            hotelDelete: null,
            errorDeleteHotel: null,
            update: null,
            hotelUpdate: {},
            errorUpdate: null,
            hotelInsert: {
                city: null,
                country: null,
                image: null,
                information: null,
                name: null,
                numberTripleRooms: null,
                numberDoubleRooms: null,
                numberStars: null
            },
            errorInsert: null,
            insert: null
        }


    }


    onClick =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }


    getAllHotels = () => {

        fetch(`https://localhost:8443/booking/hotel/listHotel`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {

                    this.setState({hotels: data, errorListHotels: false})
                }
            })
            .catch((error) => {
                this.setState({errorListHotels: true})
            });
    }


    deleteHotel = (id) => {
        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
        }
        fetch(`https://localhost:8443/booking/hotel/delete?id=${id}`, options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {

                    this.setState({hotelDelete: data, errorDeleteHotel: false})
                }
            })
            .catch((error) => {
                this.setState({errorDeleteHotel: true})
            });
        this.getAllHotels()

    }

    modifyIsOpenForm = () => {
        this.setState({update: !this.state.update})
    }

    modifyIsOpenFormInsert = () => {
        this.setState({insert: !this.state.insert})
    }


    componentWillMount() {
        this.getAllHotels();
    }


    getCity = (event) => {
        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: event.target.value,
                country: this.state.hotelUpdate.country,
                image: this.state.hotelUpdate.image,
                information: this.state.hotelUpdate.information,
                name: this.state.hotelUpdate.name,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })
    }

    getCountry = (event) => {

        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: event.target.value,
                image: this.state.hotelUpdate.image,
                information: this.state.hotelUpdate.information,
                name: this.state.hotelUpdate.name,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })
    }

    getImage = (event) => {

        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: this.state.hotelUpdate.country,
                image: event.target.value,
                information: this.state.hotelUpdate.information,
                name: this.state.hotelUpdate.name,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })

    }

    getInformation = (event) => {
        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: this.state.hotelUpdate.country,
                image: this.state.hotelUpdate.image,
                information: event.target.value,
                name: this.state.hotelUpdate.name,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })
    }


    getName = (event) => {
        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: this.state.hotelUpdate.country,
                image: this.state.hotelUpdate.image,
                information: this.state.hotelUpdate.information,
                name: event.target.value,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })
    }


    getNumberDoubleRoom = (event) => {
        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: this.state.hotelUpdate.country,
                image: this.state.hotelUpdate.image,
                information: this.state.hotelUpdate.information,
                name: this.state.hotel.name,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: event.target.value,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })
    }

    getNumberTripleRooms = (event) => {
        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: this.state.hotelUpdate.country,
                image: this.state.hotelUpdate.image,
                information: this.state.hotelUpdate.information,
                name: this.state.hotelUpdate.name,
                numberTripleRooms: event.target.value,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: this.state.hotelUpdate.numberStars
            }
        })
    }


    getNumberStars = (event) => {
        this.setState({
            hotelUpdate: {
                idHotel: this.state.hotelUpdate.idHotel,
                city: this.state.hotelUpdate.city,
                country: this.state.hotelUpdate.country,
                image: this.state.hotelUpdate.image,
                information: this.state.hotelUpdate.information,
                name: this.state.hotelUpdate.name,
                numberTripleRooms: this.state.hotelUpdate.numberTripleRooms,
                numberDoubleRooms: this.state.hotelUpdate.numberDoubleRooms,
                numberStars: event.target.value
            }
        })
    }


    getCityInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: event.target.value,
                country: this.state.hotelInsert.country,
                image: this.state.hotelInsert.image,
                information: this.state.hotelInsert.information,
                name: this.state.hotelInsert.name,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getCountryInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: this.state.hotelInsert.city,
                country: event.target.value,
                image: this.state.hotelInsert.image,
                information: this.state.hotelInsert.information,
                name: this.state.hotelInsert.name,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getImageInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: this.state.hotelInsert.city,
                country: this.state.hotelInsert.country,
                image: event.target.value,
                information: this.state.hotelInsert.information,
                name: this.state.hotelInsert.name,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getInformationInsert = (event) => {
        this.setState({
            hotelInsert: {
                city: this.state.hotelInsert.city,
                country: this.state.hotelInsert.country,
                image: this.state.hotelInsert.image,
                information: event.target.value,
                name: this.state.hotelInsert.name,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getNameInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: this.state.hotelInsert.city,
                country: this.state.hotelInsert.country,
                image: this.state.hotelInsert.image,
                information: this.state.hotelInsert.information,
                name: event.target.value,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getNumberTripleRoomsInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: this.state.hotelInsert.city,
                country: this.state.hotelInsert.country,
                image: this.state.hotelInsert.image,
                information: this.state.hotelInsert.information,
                name: this.state.hotelInsert.name,
                numberTripleRooms: event.target.value,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getNumberDoubleRoomsInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: this.state.hotelInsert.city,
                country: this.state.hotelInsert.country,
                image: this.state.hotelInsert.image,
                information: this.state.hotelInsert.information,
                name: this.state.hotelInsert.name,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: event.target.value,
                numberStars: this.state.hotelInsert.numberStars
            }
        })
    }

    getNumberStarsInsert = (event) => {
        this.setState({
            hotelInsert: {

                city: this.state.hotelInsert.city,
                country: this.state.hotelInsert.country,
                image: this.state.hotelInsert.image,
                information: this.state.hotelInsert.information,
                name: this.state.hotelInsert.name,
                numberTripleRooms: this.state.hotelInsert.numberTripleRooms,
                numberDoubleRooms: this.state.hotelInsert.numberDoubleRooms,
                numberStars: event.target.value
            }
        })
    }


    update = () => {
        const obj = {
            "idHotel": this.state.hotelUpdate.idHotel,
            "name": this.state.hotelUpdate.name,
            "city": this.state.hotelUpdate.city,
            "country": this.state.hotelUpdate.country,
            "numberStars": this.state.hotelUpdate.numberStars,
            "information": this.state.hotelUpdate.information,
            "image": this.state.hotelUpdate.image,
            "numberDoubleRooms": this.state.hotelUpdate.numberDoubleRooms,
            "numberTripleRooms": this.state.hotelUpdate.numberTripleRooms
        }

        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(obj)
        }
        fetch("https://localhost:8443/booking/hotel/edit", options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.getAllHotels()
                    this.setState({errorUpdate: false, update: false})
                }
            })
            .catch((error) => {
                this.setState({errorUpdate: true})
            });

    }


    insert = () => {
        const obj = {

            "name": this.state.hotelInsert.name,
            "city": this.state.hotelInsert.city,
            "country": this.state.hotelInsert.country,
            "numberStars": this.state.hotelInsert.numberStars,
            "information": this.state.hotelInsert.information,
            "image": this.state.hotelInsert.image,
            "numberDoubleRooms": this.state.hotelInsert.numberDoubleRooms,
            "numberTripleRooms": this.state.hotelInsert.numberTripleRooms
        }

        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(obj)
        }
        fetch("https://localhost:8443/booking/hotel/insert", options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.getAllHotels()
                    this.setState({errorInsert: false, insert: false})
                }
            })
            .catch((error) => {
                this.setState({errorInsert: true})
            });

    }


    render() {
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null || localStorage.getItem('typeUser') !== 'admin') {
            this.props.history.push("/")
            return null
        } else {
            return (

                <div>
                    <header>
                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Navbar</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClick}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem>
                                        <MDBNavLink to="/adminpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/offers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/hotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem >
                                        <MDBNavLink to="/users">Users</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon icon="sign-out"/> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>

                            </MDBCollapse>
                        </MDBNavbar>
                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">
                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-12">
                            <div>
                                <Card>
                                    <CardHeader
                                        tag="h3"
                                        className="text-center font-weight-bold text-uppercase py-4"
                                    >
                                        Hotel
                                    </CardHeader>
                                    <CardBody>
                                        <BootstrapTable data={this.state.hotels} version='4'>
                                            <TableHeaderColumn isKey dataField='idHotel' widthColumn={'3%'}>ID</TableHeaderColumn>
                                            <TableHeaderColumn dataField='city' widthColumn={'8%'}>City</TableHeaderColumn>
                                            <TableHeaderColumn dataField='country' widthColumn={'8%'}>Country</TableHeaderColumn>
                                            <TableHeaderColumn dataField='numberDoubleRooms' widthColumn={'5%'}>Number double
                                                rooms</TableHeaderColumn>
                                            <TableHeaderColumn dataField='numberTripleRooms' widthColumn={'5%'}>Number triple
                                                rooms</TableHeaderColumn>
                                            <TableHeaderColumn dataField='information'  widthColumn={'10%'}>Information</TableHeaderColumn>
                                            <TableHeaderColumn dataField='numberStars' widthColumn={'5%'}>Number stars</TableHeaderColumn>
                                            <TableHeaderColumn dataField='name' widthColumn={'10%'}>Name</TableHeaderColumn>
                                            <TableHeaderColumn dataField='image' widthColumn={'25%'}
                                                               dataFormat={(cell, row, enumObject, index) => {
                                                                   return (<img width={'35%'} height={'15%'}
                                                                                src={this.state.hotels[index].image}/>)
                                                               }}
                                            >Image</TableHeaderColumn>
                                            <TableHeaderColumn columnWidth='11%' dataFormat={(cell, row, enumObject, index) => {
                                                return (<MDBBtn gradient="aqua"
                                                                rounded
                                                                size="sm"
                                                                className="mr-auto"
                                                                onClick={() => this.deleteHotel(this.state.hotels[index].idHotel)}>
                                                    <MDBIcon
                                                        icon="window-close"/> </MDBBtn>)
                                            }}
                                            >Delete</TableHeaderColumn>
                                            <TableHeaderColumn widthColumn='10%' dataFormat={(cell, row, enumObject, index) => {
                                                return (<MDBBtn gradient="aqua"
                                                                rounded
                                                                size="sm"
                                                                className="mr-auto"
                                                                onClick={() => {
                                                                    this.setState({
                                                                        update: true,
                                                                        hotelUpdate: this.state.hotels[index]
                                                                    })
                                                                }}> <MDBIcon
                                                    icon="pencil"/> </MDBBtn>)
                                            }}
                                            >Update</TableHeaderColumn>
                                        </BootstrapTable>

                                    </CardBody>
                                </Card>
                                <MDBBtn onClick={() => {
                                    this.setState({insert: true})
                                }}> Add hotel</MDBBtn>
                                {this.state.errorDeleteHotel === true ? <Alert>
                                    Unsuccessful deletion</Alert> : null}
                                <Modal isOpen={this.state.update} toggle={this.modifyIsOpenForm}>
                                    <ModalHeader toggle={this.modifyIsOpenForm}></ModalHeader>
                                    <ModalBody style={{width: '100%'}}>
                                        <HotelForm hotel={this.state.hotelUpdate}
                                                   getCity={this.getCity}
                                                   getCountry={this.getCountry}
                                                   getName={this.getName}
                                                   getInformation={this.getInformation}
                                                   getImage={this.getImage}
                                                   getNumberStars={this.getNumberStars}
                                                   getNumberDoubleRoom={this.getNumberDoubleRoom}
                                                   getNumberTripleRooms={this.getNumberTripleRooms}
                                                   update={this.update}
                                        />
                                        {this.state.errorUpdate === true ? <Alert>
                                            Unsuccessful editing</Alert> : null}
                                    </ModalBody>
                                </Modal>

                                <Modal isOpen={this.state.insert} toggle={this.modifyIsOpenFormInsert}>
                                    <ModalHeader toggle={this.modifyIsOpenFormInsert}></ModalHeader>
                                    <ModalBody style={{width: '100%'}}>
                                        <HotelFormInsert
                                            getCity={this.getCityInsert}
                                            getCountry={this.getCountryInsert}
                                            getName={this.getNameInsert}
                                            getInformation={this.getInformationInsert}
                                            getImage={this.getImageInsert}
                                            getNumberStars={this.getNumberStarsInsert}
                                            getNumberDoubleRoom={this.getNumberDoubleRoomsInsert}
                                            getNumberTripleRooms={this.getNumberTripleRoomsInsert}
                                            update={this.update}
                                            insert={this.insert}
                                        />
                                        {this.state.errorInsert === true ? <Alert>
                                            Unsuccessful inserting</Alert> : null}
                                    </ModalBody>
                                </Modal>
                            </div>
                        </MDBContainer>
                    </main>
                </div>
            );
        }
    }
}

export default Hotels;
