import React from "react";
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";
import {MDBNavItem} from "../client/Clientpage";

const OfferForm = (props) => {
    return (

        <div>

            <MDBCard>
                <MDBCardBody>
                    <MDBCardHeader className="form-header aqua-gradient rounded">
                        <h3 >
                            Update offer:
                        </h3>
                    </MDBCardHeader>
                    <form>
                        <div className="grey-text">
                            <MDBInput
                               value={props.offer.dataEnd}
                                type="date"
                                onChange ={props.getDataEnd}
                            >End date</MDBInput>
                            <MDBInput
                                type="date"
                                value={props.offer.dateStart}
                                onChange ={props.getDataStart}
                            > Start date</MDBInput>
                            <MDBInput
                                type="number"
                                value={props.offer.numberDoubleRooms}
                                onChange ={props.getNumberDoubleRoom}
                            > Number double rooms</MDBInput>
                            <MDBInput
                                type="number"
                                value={props.offer.numberTripleRooms}
                                onChange ={props.getNumberTripleRooms}
                            >Number triple rooms</MDBInput>
                            <MDBInput
                                type="price"
                                value={props.offer.price}
                                onChange ={props.getPrice}
                            >Price</MDBInput>
                            <MDBInput
                                type="number"
                                value={props.offer.hotel.idHotel}
                                onChange ={props.getIdHotel}
                            >Id hotel</MDBInput>
                        </div>
                        <MDBBtn gradient="aqua"
                                rounded
                                size="sm"
                                className="mr-auto"
                                onClick={props.update}><MDBIcon icon="pencil" /></MDBBtn>

                    </form>
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

    OfferForm.defaultProps={
        offer:{}
    }
export default OfferForm;