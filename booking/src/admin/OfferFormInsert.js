import React from "react";
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";

const OfferFormInsert = (props) => {
    return (

        <div>

            <MDBCard>
                <MDBCardBody>
                    <MDBCardHeader className="form-header aqua-gradient rounded">
                        <h3 >
                            Insert offer:
                        </h3>
                    </MDBCardHeader>
                    <form>
                        <div className="grey-text">
                            <MDBInput
                                type="date"
                                onChange ={props.getDataEndInsert}
                            >End date</MDBInput>
                            <MDBInput
                                type="date"
                                onChange ={props.getDataStartInsert}
                            > Start date</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getNumberDoubleRoomInsert}
                            > Number double rooms</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getNumberTripleRoomsInsert}
                            >Number triple rooms</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getPriceInsert}
                            >Price</MDBInput>
                            <MDBInput
                                type="number"
                                onChange ={props.getIdHotelInsert}
                            >Id hotel</MDBInput>
                        </div>
                        <MDBBtn gradient="aqua"
                                rounded
                                size="sm"
                                className="mr-auto"
                                onClick={props.insert}><MDBIcon icon="plus" /></MDBBtn>

                    </form>
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

export default OfferFormInsert;