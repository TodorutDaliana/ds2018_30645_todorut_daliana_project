import React,{Component} from 'react';
import { Modal, ModalBody, ModalHeader,Card, CardHeader, CardBody, TableEditable,MDBTableHead,MDBTableBody,MDBTable,MDBBtn,MDBIcon  } from "mdbreact";
import {BootstrapTable,TableHeaderColumn} from 'react-bootstrap-table';

import OfferForm from "./OfferForm";
import {Alert} from "react-bootstrap";
import OfferFormInsert from "./OfferFormInsert";
import {
    MDBCollapse, MDBContainer, MDBMask,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavbarToggler,
    MDBNavItem,
    MDBNavLink, MDBView
} from "mdbreact";



class Offers extends Component {

    constructor(props){
        super(props);
        this.state={
            collapse: false,
            isWideEnough: false,
            errorListOffers: null,
            offers:[],
            offerDelete:null,
            errorDeleteOffer:null,
            update:null,
            offerUpdate:{},
            errorUpdate:null,
            offerInsert:{dataEnd:null,
                dateStart:null,
                numberTripleRooms:null,
                numberDoubleRooms:null,
                price:null,
                hotel:{idHotel: null}},
            errorInsert:null,
            insert:null
        }


    }


    onClick =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }



    getAllOffers = () => {

        fetch(`https://localhost:8443/booking/offer/allOffers`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {

                    this.setState({offers: data, errorListOffers:false})
                }
            })
            .catch((error) => {
                this.setState({errorListOffers:true})
            });
    }


    deleteOffer=(id)=>{
        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
        }
        fetch(`https://localhost:8443/booking/offer/deleteOffer?id=${id}`,options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.getAllOffers()
                    this.setState({offerDelete: data, errorDeleteOffer:false})
                }
            })
            .catch((error) => {
                this.setState({errorDeleteOffer:true})
            });

    }

    modifyIsOpenForm=()=>{
        this.setState({update:!this.state.update})
    }

    modifyIsOpenFormInsert=()=>{
        this.setState({insert:!this.state.insert})
    }


    componentWillMount() {
        this.getAllOffers();
    }


    getDataEnd=(event)=>{
        this.setState({offerUpdate:{
            idOffer:this.state.offerUpdate.idOffer,
            dataEnd:event.target.value,
            dateStart:this.state.offerUpdate.dateStart,
            numberTripleRooms:this.state.offerUpdate.numberTripleRooms,
            numberDoubleRooms:this.state.offerUpdate.numberDoubleRooms,
            price:this.state.offerUpdate.price,
            hotel:{idHotel: this.state.offerUpdate.hotel.idHotel}
            }})
    }

    getDataStart=(event)=>{
        this.setState({offerUpdate:{
                idOffer:this.state.offerUpdate.idOffer,
                dataEnd:event.target.value,
                dateStart:event.target.value,
                numberTripleRooms:this.state.offerUpdate.numberTripleRooms,
                numberDoubleRooms:this.state.offerUpdate.numberDoubleRooms,
                price:this.state.offerUpdate.price,
                hotel:{idHotel: this.state.offerUpdate.hotel.idHotel}

            }})
    }

    getNumberDoubleRoom=(event)=>{
        this.setState({offerUpdate:{
            idOffer:this.state.offerUpdate.idOffer,
            dataEnd:this.state.offerUpdate.dataEnd,
            dateStart:this.state.offerUpdate.dateStart,
            numberTripleRooms:this.state.offerUpdate.numberTripleRooms,
            numberDoubleRooms:event.target.value,
            price:this.state.offerUpdate.price,
            hotel:{idHotel: this.state.offerUpdate.hotel.idHotel}
            }})
    }

    getNumberTripleRooms=(event)=>{
        this.setState({offerUpdate:{
                idOffer:this.state.offerUpdate.idOffer,
                dataEnd:this.state.offerUpdate.dataEnd,
                dateStart:this.state.offerUpdate.dateStart,
                numberTripleRooms:event.target.value,
                numberDoubleRooms:this.state.offerUpdate.numberDoubleRooms,
                price:this.state.offerUpdate.price,
                hotel:{idHotel: this.state.offerUpdate.hotel.idHotel}
            }})
    }

    getPrice=(event)=>{
        this.setState({offerUpdate:{
                idOffer:this.state.offerUpdate.idOffer,
                dataEnd:this.state.offerUpdate.dataEnd,
                dateStart:this.state.offerUpdate.dateStart,
                numberTripleRooms:this.state.offerUpdate.numberTripleRooms,
                numberDoubleRooms:this.state.offerUpdate.numberDoubleRooms,
                price:event.target.value,
                hotel:{idHotel: this.state.offerUpdate.hotel.idHotel}
            }})
    }

    getIdHotel=(event)=>{
        this.setState({offerUpdate:{
                idOffer:this.state.offerUpdate.idOffer,
                dataEnd:this.state.offerUpdate.dataEnd,
                dateStart:this.state.offerUpdate.dateStart,
                numberTripleRooms:this.state.offerUpdate.numberTripleRooms,
                numberDoubleRooms:this.state.offerUpdate.numberDoubleRooms,
                price:this.state.offerUpdate.price,
                hotel:{idHotel: event.target.value}
            }})
    }

    getDataEndInsert=(event)=>{
        this.setState({offerInsert:{

                dataEnd:event.target.value,
                dateStart:this.state.offerInsert.dateStart,
                numberTripleRooms:this.state.offerInsert.numberTripleRooms,
                numberDoubleRooms:this.state.offerInsert.numberDoubleRooms,
                price:this.state.offerInsert.price,
                hotel:{idHotel: this.state.offerInsert.hotel.idHotel}
            }})
    }

    getDataStartInsert=(event)=>{
        this.setState({offerInsert:{

                dataEnd:this.state.offerInsert.dataEnd,
                dateStart:event.target.value,
                numberTripleRooms:this.state.offerInsert.numberTripleRooms,
                numberDoubleRooms:this.state.offerInsert.numberDoubleRooms,
                price:this.state.offerInsert.price,
                hotel:{idHotel: this.state.offerInsert.hotel.idHotel}

            }})
    }

    getNumberDoubleRoomInsert=(event)=>{
        this.setState({offerInsert:{

                dataEnd:this.state.offerInsert.dataEnd,
                dateStart:this.state.offerInsert.dateStart,
                numberTripleRooms:this.state.offerInsert.numberTripleRooms,
                numberDoubleRooms:event.target.value,
                price:this.state.offerInsert.price,
                hotel:{idHotel: this.state.offerInsert.hotel.idHotel}
            }})
    }

    getNumberTripleRoomsInsert=(event)=>{
        this.setState({offerInsert:{

                dataEnd:this.state.offerInsert.dataEnd,
                dateStart:this.state.offerInsert.dateStart,
                numberTripleRooms:event.target.value,
                numberDoubleRooms:this.state.offerInsert.numberDoubleRooms,
                price:this.state.offerInsert.price,
                hotel:{idHotel: this.state.offerInsert.hotel.idHotel}
            }})
    }

    getPriceInsert=(event)=>{
        this.setState({offerInsert:{

                dataEnd:this.state.offerInsert.dataEnd,
                dateStart:this.state.offerInsert.dateStart,
                numberTripleRooms:this.state.offerInsert.numberTripleRooms,
                numberDoubleRooms:this.state.offerInsert.numberDoubleRooms,
                price:event.target.value,
                hotel:{idHotel: this.state.offerInsert.hotel.idHotel}
            }})
    }

    getIdHotelInsert=(event)=>{
        this.setState({offerInsert:{

                dataEnd:this.state.offerInsert.dataEnd,
                dateStart:this.state.offerInsert.dateStart,
                numberTripleRooms:this.state.offerInsert.numberTripleRooms,
                numberDoubleRooms:this.state.offerInsert.numberDoubleRooms,
                price:this.state.offerInsert.price,
                hotel:{idHotel: event.target.value}
            }})
    }


    update=()=>{
         const obj={
            "idOffer": this.state.offerUpdate.idOffer,
            "price":this.state.offerUpdate.price,
            "dateStart": this.state.offerUpdate.dateStart,
            "dataEnd": this.state.offerUpdate.dataEnd,
            "hotel": this.state.offerUpdate.hotel,
            "numberDoubleRooms": this.state.offerUpdate.numberDoubleRooms,
            "numberTripleRooms": this.state.offerUpdate.numberTripleRooms
        }

        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(obj)
        }
        fetch("https://localhost:8443/booking/offer/updateOffer", options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.getAllOffers()
                    this.setState({errorUpdate: false,update:false})
                }
            })
            .catch((error) => {
                this.setState({errorUpdate: true})
            });

    }


    insert=()=>{
        const obj={

            "price":this.state.offerInsert.price,
            "dateStart": this.state.offerInsert.dateStart,
            "dataEnd": this.state.offerInsert.dataEnd,
            "hotel": this.state.offerInsert.hotel,
            "numberDoubleRooms": this.state.offerInsert.numberDoubleRooms,
            "numberTripleRooms": this.state.offerInsert.numberTripleRooms
        }

        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(obj)
        }
        fetch("https://localhost:8443/booking/offer/insert", options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.getAllOffers()
                    this.setState({errorInsert: false,insert:false})
                }
            })
            .catch((error) => {
                this.setState({errorInsert: true})
            });

    }


    render(){
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null || localStorage.getItem('typeUser')!== 'admin') {
            this.props.history.push("/")
            return null
        } else {
            return (

                <div>
                    <header>
                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Navbar</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClick}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem >
                                        <MDBNavLink to="/adminpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/offers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/hotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/users">Users</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon icon="sign-out" /> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>

                            </MDBCollapse>
                        </MDBNavbar>
                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">
                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-5">
                            <div>
                                <Card>
                                    <CardHeader
                                        tag="h3"
                                        className="text-center font-weight-bold text-uppercase py-4"
                                    >
                                        Offer
                                    </CardHeader>
                                    <CardBody>
                                        <BootstrapTable data={this.state.offers} version='4'>
                                            <TableHeaderColumn isKey dataField='idOffer'>Offer ID</TableHeaderColumn>
                                            <TableHeaderColumn dataField='dataEnd'>End date</TableHeaderColumn>
                                            <TableHeaderColumn dataField='dateStart'>Start date</TableHeaderColumn>
                                            <TableHeaderColumn dataField='numberDoubleRooms'>Number double
                                                rooms</TableHeaderColumn>
                                            <TableHeaderColumn dataField='numberTripleRooms'>Number triple
                                                rooms</TableHeaderColumn>
                                            <TableHeaderColumn dataField='price'>Price</TableHeaderColumn>
                                            <TableHeaderColumn dataField='hotel.name'
                                                               dataFormat={(cell, row, enumObject, index) => {
                                                                   return (
                                                                       <div>{this.state.offers[index].hotel.name}</div>)
                                                               }}
                                            >Hotel Name</TableHeaderColumn>
                                            <TableHeaderColumn dataFormat={(cell, row, enumObject, index) => {
                                                return (<MDBBtn gradient="aqua"
                                                                rounded
                                                                size="sm"
                                                                className="mr-auto"
                                                                onClick={() => this.deleteOffer(this.state.offers[index].idOffer)}>
                                                    <MDBIcon
                                                        icon="window-close"/> </MDBBtn>)
                                            }}
                                            >Delete</TableHeaderColumn>
                                            <TableHeaderColumn dataFormat={(cell, row, enumObject, index) => {
                                                return (<MDBBtn gradient="aqua"
                                                                rounded
                                                                size="sm"
                                                                className="mr-auto"
                                                                onClick={() => {
                                                                    this.setState({
                                                                        update: true,
                                                                        offerUpdate: this.state.offers[index]
                                                                    })
                                                                }}> <MDBIcon
                                                    icon="pencil"/> </MDBBtn>)
                                            }}
                                            >Update</TableHeaderColumn>
                                        </BootstrapTable>

                                    </CardBody>
                                </Card>
                                <MDBBtn onClick={() => {
                                    this.setState({insert: true})
                                }}> Add offer</MDBBtn>
                                {this.state.errorDeleteOffer === true ? <Alert>
                                    Unsuccessful deletion</Alert> : null}
                                <Modal isOpen={this.state.update} toggle={this.modifyIsOpenForm}>
                                    <ModalHeader toggle={this.modifyIsOpenForm}></ModalHeader>
                                    <ModalBody style={{width: '100%'}}>
                                        <OfferForm offer={this.state.offerUpdate}
                                                   getDataEnd={this.getDataEnd}
                                                   getDataStart={this.getDataStart}
                                                   getNumberDoubleRoom={this.getNumberDoubleRoom}
                                                   getNumberTripleRooms={this.getNumberTripleRooms}
                                                   getPrice={this.getPrice}
                                                   getIdHotel={this.getIdHotel}
                                                   update={this.update}
                                        />
                                        {this.state.errorUpdate === true ? <Alert>
                                            Unsuccessful editing</Alert> : null}
                                    </ModalBody>
                                </Modal>

                                <Modal isOpen={this.state.insert} toggle={this.modifyIsOpenFormInsert}>
                                    <ModalHeader toggle={this.modifyIsOpenFormInsert}></ModalHeader>
                                    <ModalBody style={{width: '100%'}}>
                                        <OfferFormInsert
                                            getDataEndInsert={this.getDataEndInsert}
                                            getDataStartInsert={this.getDataStartInsert}
                                            getNumberDoubleRoomInsert={this.getNumberDoubleRoomInsert}
                                            getNumberTripleRoomsInsert={this.getNumberTripleRoomsInsert}
                                            getPriceInsert={this.getPriceInsert}
                                            getIdHotelInsert={this.getIdHotelInsert}
                                            insert={this.insert}
                                        />
                                        {this.state.errorInsert === true ? <Alert>
                                            Unsuccessful inserting</Alert> : null}
                                    </ModalBody>
                                </Modal>
                            </div>
                        </MDBContainer>
                    </main>
                </div>


            );
        }
    }
}
export default  Offers;
