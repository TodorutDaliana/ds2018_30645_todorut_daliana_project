import React from "react";
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";


const UserForm = (props) => {
    return (

        <div>

            <MDBCard>
                <MDBCardBody>
                    <MDBCardHeader className="form-header aqua-gradient rounded">
                        <h3 >
                            Update user:
                        </h3>
                    </MDBCardHeader>
                    <form>
                        <div className="grey-text">
                            <MDBInput
                                value={props.user.firstname}
                                type="name"
                                onChange ={props.getFirstName}
                            >First name</MDBInput>
                            <MDBInput
                                type="name"
                                value={props.user.lastname}
                                onChange ={props.getLastName}
                            > Last name</MDBInput>
                            <MDBInput
                                type="name"
                                value={props.user.password}
                                onChange ={props.getPassword}
                            > Password</MDBInput>
                            <MDBInput
                                type="name"
                                value={props.user.typeUser}
                                onChange ={props.getTypeUser}
                            > Type user</MDBInput>
                            <MDBInput
                                type="boolean"
                                value={props.user.active}
                                onChange ={props.getActive}
                            >Active</MDBInput>
                            <MDBInput
                                type="phone number"
                                value={props.user.telephoneNumber}
                                onChange ={props.getTelephoneNumber}
                            >Telephone number</MDBInput>
                            <MDBInput
                                type="email"
                                value={props.user.email}
                                onChange ={props.getEmail}
                            >Email</MDBInput>
                        </div>
                        <MDBBtn gradient="aqua"
                                rounded
                                size="sm"
                                className="mr-auto"
                                onClick={props.update}><MDBIcon icon="pencil" /></MDBBtn>

                    </form>
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

UserForm.defaultProps={
    user:{}
}
export default UserForm;