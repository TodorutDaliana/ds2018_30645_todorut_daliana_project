import React, {Component} from 'react';
import {
    Modal,
    ModalBody,
    ModalHeader,
    Card,
    CardHeader,
    CardBody,
    TableEditable,
    MDBTableHead,
    MDBTableBody,
    MDBTable,
    MDBBtn,
    MDBIcon,
    MDBCollapse, MDBContainer, MDBMask,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavbarToggler,
    MDBNavItem,
    MDBNavLink,
    MDBView
} from "mdbreact";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Alert} from "react-bootstrap";
import UserForm from "./UserForm";



class Hotels extends Component {

    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
            errorListUsers: null,
            users: [],
            userDelete: null,
            errorDeleteUser: null,
            update: null,
            userUpdate: {},
            errorUpdate: null,
        }
    }

    onClick =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }


    getAllUsers = () => {

        fetch(`https://localhost:8443/booking/user/allUsers`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {

                    this.setState({users: data, errorListUsers: false})
                }
            })
            .catch((error) => {
                this.setState({errorListUsers: true})
            });
    }


    deleteUser = (email) => {
        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
        }
        fetch(`https://localhost:8443/booking/user/deactivateUser?email=${email}`, options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                     this.getAllUsers()
                    this.setState({userDelete: data, errorDeleteUser: false})
                }
            })
            .catch((error) => {
                this.setState({errorDeleteUser: true})
            });


    }

    modifyIsOpenForm = () => {
        this.setState({update: !this.state.update})
    }


    componentWillMount() {
        this.getAllUsers();
    }


    getActive = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: this.state.userUpdate.email,
                active: event.target.value,
                firstname: this.state.userUpdate.firstname,
                lastname: this.state.userUpdate.lastname,
                telephoneNumber: this.state.userUpdate.telephoneNumber,
                password: this.state.userUpdate.password,
                typeUser: this.state.userUpdate.typeUser
            }
        })
    }

    getEmail = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: event.target.value,
                active: this.state.userUpdate.active,
                firstname: this.state.userUpdate.firstname,
                lastname: this.state.userUpdate.lastname,
                telephoneNumber: this.state.userUpdate.telephoneNumber,
                password: this.state.userUpdate.password,
                typeUser: this.state.userUpdate.typeUser
            }
        })
    }

    getFirstname = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: this.state.userUpdate.email,
                active: this.state.userUpdate.active,
                firstname: event.target.value,
                lastname: this.state.userUpdate.lastname,
                telephoneNumber: this.state.userUpdate.telephoneNumber,
                password: this.state.userUpdate.password,
                typeUser: this.state.userUpdate.typeUser
            }
        })
    }

    getLastname = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: this.state.userUpdate.email,
                active: this.state.userUpdate.active,
                firstname: this.state.userUpdate.firstname,
                lastname: event.target.value,
                telephoneNumber: this.state.userUpdate.telephoneNumber,
                password: this.state.userUpdate.password,
                typeUser: this.state.userUpdate.typeUser
            }
        })
    }

    getTelephoneNumber = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: this.state.userUpdate.email,
                active: this.state.userUpdate.active,
                firstname: this.state.userUpdate.firstname,
                lastname: this.state.userUpdate.lastname,
                telephoneNumber: event.target.value,
                password: this.state.userUpdate.password,
                typeUser: this.state.userUpdate.typeUser
            }
        })
    }


    getPassword = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: this.state.userUpdate.email,
                active: this.state.userUpdate.active,
                firstname: this.state.userUpdate.firstname,
                lastname: this.state.userUpdate.lastname,
                telephoneNumber: this.state.userUpdate.telephoneNumber,
                password: event.target.value,
                typeUser: this.state.userUpdate.typeUser
            }
        })
    }


    getTypeUser = (event) => {
        this.setState({
            userUpdate: {
                id: this.state.userUpdate.id,
                email: this.state.userUpdate.email,
                active: this.state.userUpdate.active,
                firstname: this.state.userUpdate.firstname,
                lastname: this.state.userUpdate.lastname,
                telephoneNumber: this.state.userUpdate.telephoneNumber,
                password: this.state.userUpdate.password,
                typeUser: event.target.value
            }
        })
    }


    update = () => {
        const obj = {

            "id": this.state.userUpdate.id,
            "firstname": this.state.userUpdate.firstname,
            "email": this.state.userUpdate.email,
            "telephoneNumber": this.state.userUpdate.telephoneNumber,
            "lastname": this.state.userUpdate.lastname,
            "typeUser": this.state.userUpdate.typeUser,
            "password": this.state.userUpdate.password,
            "active": this.state.userUpdate.active
        }

        const options = {

            method: 'post',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(obj)
        }
        fetch("https://localhost:8443/booking/user/editUser", options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.getAllUsers()
                    this.setState({errorUpdate: false, update: false})
                }
            })
            .catch((error) => {
                this.setState({errorUpdate: true})
            });

    }


    render() {
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null || localStorage.getItem('typeUser') !== 'admin') {
            this.props.history.push("/")
            return null
        } else {
            return (

                <div>
                    <header>
                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Navbar</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClick}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem>
                                        <MDBNavLink to="/adminpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem >
                                        <MDBNavLink to="/offers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/hotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/users">Users</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon icon="sign-out"/> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>

                            </MDBCollapse>
                        </MDBNavbar>
                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">
                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-12">
                            <br/>
                            <div>
                                <Card>
                                    <CardHeader
                                        tag="h3"
                                        className="text-center font-weight-bold text-uppercase py-4"
                                    >
                                        User
                                    </CardHeader>
                                    <CardBody>
                                        <BootstrapTable data={this.state.users} version='4'>
                                            <TableHeaderColumn isKey dataField='id' >User ID</TableHeaderColumn>
                                            <TableHeaderColumn dataField='firstname' > First name</TableHeaderColumn>
                                            <TableHeaderColumn dataField='lastname' >Last name</TableHeaderColumn>
                                            <TableHeaderColumn dataField='active'  >Active</TableHeaderColumn>
                                            <TableHeaderColumn dataField='typeUser' >Type user</TableHeaderColumn>
                                            <TableHeaderColumn dataField='password'  >Password</TableHeaderColumn>
                                            <TableHeaderColumn dataField='telephoneNumber' >Telephone
                                                number</TableHeaderColumn>
                                            <TableHeaderColumn dataField='email' >Email</TableHeaderColumn>
                                            <TableHeaderColumn    dataFormat={(cell, row, enumObject, index) => {
                                                return (<MDBBtn gradient="aqua"
                                                                rounded
                                                                size="sm"
                                                                className="mr-auto"
                                                                onClick={() => this.deleteUser(this.state.users[index].email)}>
                                                    <MDBIcon
                                                        icon="window-close"/> </MDBBtn>)
                                            }}
                                            >Delete</TableHeaderColumn>
                                            <TableHeaderColumn    dataFormat={(cell, row, enumObject, index) => {
                                                return (<MDBBtn gradient="aqua"
                                                                rounded
                                                                size="sm"
                                                                className="mr-auto"
                                                                onClick={() => {
                                                                    this.setState({
                                                                        update: true,
                                                                        userUpdate: this.state.users[index]
                                                                    })
                                                                }}> <MDBIcon
                                                    icon="pencil"/> </MDBBtn>)
                                            }}
                                            >Update</TableHeaderColumn>
                                        </BootstrapTable>

                                    </CardBody>
                                </Card>

                                {this.state.errorDeleteUser === true ? <Alert>
                                    Unsuccessful deletion</Alert> : null}
                                <Modal isOpen={this.state.update} toggle={this.modifyIsOpenForm}>
                                    <ModalHeader toggle={this.modifyIsOpenForm}></ModalHeader>
                                    <ModalBody style={{width: '100%'}}>
                                        <UserForm user={this.state.userUpdate}
                                                  getLastname={this.getLastname}
                                                  getFirstname={this.getFirstname}
                                                  getPassword={this.getPassword}
                                                  getActive={this.getActive}
                                                  getTypeUser={this.getTypeUser}
                                                  getEmail={this.getEmail}
                                                  getTelephoneNumber={this.getTelephoneNumber}
                                                  update={this.update}
                                        />
                                        {this.state.errorUpdate === true ? <Alert>
                                            Unsuccessful editing</Alert> : null}
                                    </ModalBody>
                                </Modal>

                            </div>
                        </MDBContainer>
                    </main>
                </div>
            );
        }
    }
}

export default Hotels;
