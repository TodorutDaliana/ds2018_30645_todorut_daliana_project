import React,{Component} from 'react';
import {ModalBody,MDBCard, MDBCardBody, MDBCardHeader, MDBIcon, MDBInput,Row,Button, Card, CardBody, CardImage, CardTitle, CardText, Col,MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem,FormInline, MDBNavLink, MDBContainer, MDBMask, MDBView,Fa,MDBBtn } from 'mdbreact';
import {Alert} from "react-bootstrap";
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import moment from "moment";



class AllOffers extends Component {
    constructor(props){
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
            searchHotel:false,
            offers:[],
            errorListOffers:null,
            idHotel:'',
            errorDate:null,
            endDate:moment(new Date()).format('YYYY-MM-DD'),
            startDate:moment(new Date()).format('YYYY-MM-DD'),
            city:null,
            numberRoom:null,
            errorRes:null
        }
        let typeRoom;
        let offer;
        let labelInput;
        let srcMap

    }


    onClickfc =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    getPickerValueStart = (value) =>{
        if(moment(value).format('YYYY-MM-DD')>this.state.endDate) {
            this.setState({errorDate: true})
        }else{
            this.setState({startDate:moment(value).format('YYYY-MM-DD'),errorDate:false})
        }
    }

    getPickerValueEnd = (value) =>{

        if(this.state.startDate== null ||this.state.startDate > moment(value).format('YYYY-MM-DD')){
            this.setState({errorDate:true})
        }else{
            this.setState({errorDate:false,endDate:moment(value).format('YYYY-MM-DD')})
        }
    }

    getCity = (event) =>{
        this.setState({city:event.target.value})
    }


    getAllOffers = () => {


       if(this.state.errorDate == false){


            if(this.state.city !== null ){
                this.srcMap= `https://maps.google.com/maps?q=${this.state.city}&t=&z=13&ie=UTF8&iwloc=&output=embed`
        fetch(`https://localhost:8443/booking/offer/offersByDateandCity?dateStart=${this.state.startDate}&dateEnd=${this.state.endDate}&city=${this.state.city}`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {

                    this.setState({offers: data, errorListOffers:false})
                }
            })
            .catch((error) => {
                this.setState({errorListOffers:true})
            });
            }else{
                fetch(`https://localhost:8443/booking/offer/offersByDate?dateStart=${this.state.startDate}&dateEnd=${this.state.endDate}`)
                    .then(response => {
                        if (response.status >= 400) {
                            throw new Error("error");
                        }
                        return response.json()
                    })
                    .then((data) => {
                        if (data !== {}) {

                            this.setState({offers: data, errorListOffers:false})
                        }
                    })
                    .catch((error) => {
                        this.setState({errorListOffers:true})
                    });

            }
        }
    }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }

    getNumber = (event) =>{

            this.setState({numberRoom:event.target.value})

    }

    makeBook=(offer)=> {

             let obj;
            if(this.type == 'Double rooms'){
             obj = {
                "user": {
                    "id": parseInt(localStorage.getItem('userId')),
                    "firstname":localStorage.getItem('firtsname'),
                    "email": localStorage.getItem('email'),
                    "telephoneNumber": localStorage.getItem('telephoneNumber'),
                    "lastname": localStorage.getItem('name'),
                    "typeUser": 'client',
                    "password": localStorage.getItem('password'),
                    "active": localStorage.getItem('active')
                },
                 "numberDoubleRoom":  parseInt(this.state.numberRoom),
                 "numberTripleRoom":0,
                "offer": {
                    "idOffer": offer.idOffer,
                    "price": offer.price,
                    "dateStart": offer.dateStart,
                    "dataEnd": offer.dataEnd,
                    "hotel":
                        {
                            "idHotel": offer.hotel.idHotel,
                            "name": offer.hotel.name,
                            "city": offer.hotel.city,
                            "country": offer.hotel.country,
                            "numberStars": offer.hotel.numberStars,
                            "information": offer.hotel.information,
                            "image": offer.hotel.image,
                            "numberDoubleRooms": offer.hotel.numberDoubleRooms,
                            "numberTripleRooms": offer.hotel.numberTripleRooms
                        },
                    "numberDoubleRooms": offer.numberDoubleRooms,
                    "numberTripleRooms": offer.numberTripleRooms
                }
            }}else{
                obj = {
                    "user": {
                        "id": parseInt(localStorage.getItem('userId')),
                        "firstname":localStorage.getItem('firtsname'),
                        "email": localStorage.getItem('email'),
                        "telephoneNumber": localStorage.getItem('telephoneNumber'),
                        "lastname": localStorage.getItem('name'),
                        "typeUser": 'client',
                        "password": localStorage.getItem('password'),
                        "active": localStorage.getItem('active')
                    },
                    "numberDoubleRoom": 0,
                    "numberTripleRoom": parseInt(this.state.numberRoom),
                    "offer": {
                        "idOffer": offer.idOffer,
                        "price": offer.price,
                        "dateStart": offer.dateStart,
                        "dataEnd": offer.dataEnd,
                        "hotel":
                            {
                                "idHotel": offer.hotel.idHotel,
                                "name": offer.hotel.name,
                                "city": offer.hotel.city,
                                "country": offer.hotel.country,
                                "numberStars": offer.hotel.numberStars,
                                "information": offer.hotel.information,
                                "image": offer.hotel.image,
                                "numberDoubleRooms": offer.hotel.numberDoubleRooms,
                                "numberTripleRooms": offer.hotel.numberTripleRooms
                            },
                        "numberDoubleRooms": offer.numberDoubleRooms,
                        "numberTripleRooms": offer.numberTripleRooms
                    }
                }

            }


            const options = {
                mode: 'cors',
                method: 'post',
                withCredentials: true,
                crossdomain: true,
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(obj)
            }
            fetch("https://localhost:8443/booking/rezervation/insert", options)
                .then(response => {
                    if (response.status >= 400) {
                        throw new Error("error");
                    }
                    return response.json()
                })
                .then((data) => {
                    if (data !== {}) {
                        this.getAllOffers()
                        this.setState({userObject: data, errorRes: false})
                    }
                })
                .catch((error) => {
                    this.setState({errorRes: true})
                });

    }


    componentWillMount() {
        this.getAllOffers();

    }


    render() {
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null) {
            this.props.history.push("/")
            return null
        } else {

            return (

                <div>
                    <header>

                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Fist Page</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClickfc}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem>
                                        <MDBNavLink to="/clientpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewhotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/viewalloffers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem >
                                        <MDBNavLink to="/viewallreservations">Reservations</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon className="text-white"
                                                                           icon="sign-out"/> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                            </MDBCollapse>
                        </MDBNavbar>

                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">

                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-5">
                                <div>
                                    <MDBCard >
                                        <MDBCardBody>
                                            <form>
                                                {this.state.errorDate== true ?<span style={{color: "red"}}>{'Start date cannot be bigger than end date'}</span>:null}
                                                <div className={'grey-text'}> Check-in: <DatePicker
                                                    selected={this.state.startDate}
                                                    onChange={this.getPickerValueStart}
                                                    dateFormat={'yyyy-MM-dd'}
                                                />
                                                </div>
                                                <br/>
                                                <div className={'grey-text'}>Check-out: <DatePicker
                                                    selected={this.state.endDate}
                                                    onChange={this.getPickerValueEnd}
                                                    dateFormat={'yyyy-MM-dd'}
                                                />
                                                </div>
                                                <div className="grey-text" style={{marginLeft:'80px',marginRight:'80px'}}>

                                                    <MDBInput
                                                        label="Type city"
                                                        icon="city"
                                                        group
                                                        type="text"
                                                        validate
                                                        onChange={this.getCity}
                                                    />
                                                </div>
                                                {this.state.errorListOffers===true ?
                                                    <Alert bsStyle="warning">
                                                       Invalid input (City).
                                                    </Alert> : null}
                                                {this.state.offers.length === 0 ?
                                                    <Alert bsStyle="warning">
                                                        There are not offers. Please select another period.
                                                    </Alert> : null}
                                                <MDBBtn gradient="aqua"
                                                        rounded
                                                        size="sm"
                                                        className="mr-auto"
                                                        onClick={this.getAllOffers}><MDBIcon className="text-white"
                                                                                        icon="search"/> </MDBBtn>

                                            </form>
                                        </MDBCardBody>
                                    </MDBCard>
                                    <br/>

                                    {this.state.city && this.srcMap ?
                                        <div >
                                            <iframe src={this.srcMap}
                                                    style={{border:'0', width:'100%',height:'100%'}}></iframe>
                                        </div>  : null}
                                    {this.state.errorRes === true ?
                                        <Alert bsStyle="warning">
                                            There are not enough rooms available!
                                        </Alert> : null}
                                    {this.state.errorRes === false ? <Alert>
                                            Booked successfully</Alert> : null}
                                <br/>

                                    <Row> {this.state.offers.map((offer) => {

                                        return (
                                            <Col>
                                                {
                                                    offer.numberDoubleRooms === 0 && offer.numberTripleRooms!== 0 ?
                                                        this.type = 'Triple rooms' : null
                                                }
                                                {
                                                    offer.numberDoubleRooms !== 0 && offer.numberTripleRooms === 0 ?
                                                        this.type = 'Double rooms' : null
                                                }
                                                {
                                                    offer.numberDoubleRooms !== 0 && offer.numberTripleRooms !== 0 ?
                                                        this.type = 'Double rooms' : null
                                                }
                                                <br/>
                                                {offer.numberDoubleRooms === 0 && offer.numberTripleRooms === 0?
                                                    null :
                                                <Card style={{width: "15rem"}}>
                                                    <CardImage
                                                        className="img-fluid"
                                                        src={offer.hotel.image}
                                                        waves
                                                    />
                                                    <CardBody>
                                                        <CardTitle>Offer</CardTitle>
                                                        <CardText>
                                                            <h3>{offer.price}RON</h3>
                                                            <br/>
                                                            <h5>Hotel {offer.hotel.name} </h5>
                                                            <br/>
                                                            {offer.numberDoubleRooms ===0 && offer.numberTripleRooms!==0 ?
                                                            <p> Just Triple rooms</p>: null}
                                                            {offer.numberDoubleRooms !==0 && offer.numberTripleRooms===0 ?
                                                                <p> Just Double rooms</p>: null}
                                                            {offer.numberDoubleRooms !==0 && offer.numberTripleRooms!==0 ?
                                                                <p> Just Double rooms</p>: null}
                                                            <form>
                                                            <div className="grey-text">
                                                                <MDBInput
                                                                    label={this.type}
                                                                    icon="room"
                                                                    group
                                                                    type="number"
                                                                    validate
                                                                    onChange={this.getNumber}
                                                                />
                                                            </div>

                                                            <MDBBtn gradient="aqua"
                                                                    rounded
                                                                    size="sm"
                                                                    className="mr-auto"
                                                                    onClick={()=>this.makeBook(offer)}>Reserve </MDBBtn>
                                                        </form>
                                                        </CardText>

                                                    </CardBody>
                                                </Card>}
                                            </Col>)
                                    })}

                                    </Row>

                                </div>
                            </MDBContainer>
                    </main>

                </div>

            )
        }
    }
}

export default AllOffers;
