import React,{Component} from 'react'
import {ModalBody,MDBCard, MDBCardBody, MDBCardHeader, MDBIcon, MDBInput,Row,Button, Card, CardBody, CardImage, CardTitle, CardText, Col,MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem,FormInline, MDBNavLink, MDBContainer, MDBMask, MDBView,Fa,MDBBtn } from 'mdbreact';
import {Alert} from "react-bootstrap";

class AllReservations extends Component {

    constructor(props) {
        super(props);
        this.state = {
            reservations: [],
            errorViewReservatios: null,
            collapse: false,
            isWideEnough: false
        }
    }

    onClickfc =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    getAllReservations = () => {

        fetch(`https://localhost:8443/booking/rezervation/allRezervationUser?email=${localStorage.getItem('email')}`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {

                    this.setState({reservations: data, errorViewReservations: false})
                }
            })
            .catch((error) => {
                this.setState({errorViewReservations: true})
            });


    }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }

    componentWillMount() {
        this.getAllReservations();
    }


    render() {
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null) {
            this.props.history.push("/")
            return null
        } else {

            return (

                <div>
                    <header>

                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Fist Page</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClickfc}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem>
                                        <MDBNavLink to="/clientpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewhotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewalloffers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/viewallreservations">Reservations</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon className="text-white"
                                                                                icon="sign-out"/> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                            </MDBCollapse>
                        </MDBNavbar>

                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">

                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-5">

                            {this.state.errorViewReservations === true ?
                                <Alert bsStyle="warning">
                                    .
                                </Alert> : null}
                            <br/>
                            <Row> {this.state.reservations.map((reservation) => {

                                return (

                                    <Col>
                                        <br/>
                                        <Card style={{width: "15rem"}}>
                                            <CardImage
                                                className="img-fluid"
                                                src={reservation.offers.hotel.image}
                                                waves
                                            />
                                            <CardBody>
                                                <CardTitle>Book</CardTitle>
                                                <CardText>
                                                    {reservation.numberTripleRoom === 0 ?
                                                        <h3>{reservation.numberDoubleRoom}x{reservation.offers.price}RON</h3> :
                                                        <h3>{reservation.numberTripleRoom}x{reservation.offers.price}RON</h3>}
                                                    <br/>
                                                    <h5>Hotel {reservation.offers.hotel.name} </h5>
                                                    <br/>
                                                    {reservation.numberTripleRoom === 0 ?
                                                        <h3>Double rooms: {reservation.numberDoubleRoom}</h3> :
                                                        <h3>Triple rooms:{reservation.numberTripleRoom}</h3>}
                                                    <br/>
                                                    <h5>{reservation.offers.hotel.city},{reservation.offers.hotel.country}</h5>
                                                </CardText>

                                            </CardBody>
                                        </Card>
                                    </Col>)
                            })}

                            </Row>
                        </MDBContainer>
                    </main>
                </div>
            )
        }

    }
}
export default AllReservations;
