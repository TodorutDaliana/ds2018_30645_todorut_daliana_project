import React,{Component} from 'react';
//import "font-awesome/css/font-awesome.min.css";
//import "bootstrap-css-only/css/bootstrap.min.css";
//import "mdbreact/dist/css/mdb.css";

import {MDBIcon,Modal, ModalBody, ModalHeader,MDBDatePicker,Row,Button, Card, CardBody, CardImage, CardTitle, CardText, Col,MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem,FormInline, MDBNavLink, MDBContainer, MDBMask, MDBView,Fa,MDBBtn } from 'mdbreact';
import {Alert} from "react-bootstrap";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
import moment from "moment";



class ViewHotels extends Component{
    constructor(props){
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
            nameHotel: '',
            hotel: null,
            errorSelectHotel: null,
            searchHotel:false,
            hotels:[],
            errorListHotels:null,
            startDate:new Date(),
            endDate:new Date(),
            isOpenDatepicker:false,
            errorDate:null
        }
        localStorage.setItem('idHotel','')
    }

    onClickfc =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    getNameHotel=(event)=>{
        if(event.target.value === ''){
            this.setState({errorSelectHotel:false})
        }
        this.setState({nameHotel:event.target.value})
    }

    getHotelByName = () => {

        fetch(`http://localhost:8080/booking/hotel/findByName?name=${this.state.nameHotel}`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    console.log(data.name)
                    this.setState({hotel: data,errorSelectHotel:false, searchHotel:true})
                }
            })
            .catch((error) => {
                this.setState({errorSelectHotel : true,searchHotel:false})
            });
    }




    getAllHotel = () => {
        if(this.state.searchHotel === false) {
            fetch(`https://localhost:8443/booking/hotel/listHotel`)
                .then(response => {
                    if (response.status >= 400) {
                        throw new Error("error");
                    }
                    return response.json()
                })
                .then((data) => {
                    if (data !== {}) {
                        this.setState({hotels: data, errorListHotels:false})
                    }
                })
                .catch((error) => {
                    this.setState({errorListHotels:true})
                });
        }
    }

    getOffers=(id,image) =>{
        localStorage.setItem('idHotel',id);
        localStorage.setItem('image',image);
        this.setState({isOpenDatepicker:true})

    }

    signout = () => {
      localStorage.clear();
      this.props.history.push('/')
    }

    getPickerValueStart = (value) =>{
        if(moment(value).format('YYYY-MM-DD')>this.state.endDate) {
            this.setState({errorDate: true})
        }else{
            this.setState({startDate:moment(value).format('YYYY-MM-DD'),errorDate:false})
        }
    }

    getPickerValueEnd = (value) =>{

        if(this.state.startDate== null ||this.state.startDate > moment(value).format('YYYY-MM-DD')){
            this.setState({errorDate:true})
        }else{
            this.setState({errorDate:false,endDate:moment(value).format('YYYY-MM-DD')})
        }
    }

    goToOffer = () =>{
        if(this.state.errorDate === false){
        this.setState({isOpenDatepicker:false});
        localStorage.setItem('startDate',this.state.startDate);
        localStorage.setItem('endDate',this.state.endDate);
        this.props.history.push('/viewOffers')}
    }

    modifyIsOpenDate = ()=>{
       this.setState({isOpenDatepicker:!this.state.isOpenDatepicker})
    }

    componentWillMount(){
        this.getAllHotel();
    }

    render() {
        console.log(this.state.errorDate)
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null) {
            this.props.history.push("/")
            return null
        } else {

            return (


                <div>
                    <header>

                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Navbar</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClickfc}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem>
                                        <MDBNavLink to="/clientpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/viewhotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewallreservations">Reservations</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/offers">OffersAdmin</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem className={'right'} style={{alignContent: 'right'}}>
                                        <div className="md-form my-0">
                                            <FormInline>
                                                <input
                                                    className="form-control mr-sm-2"
                                                    type="text"
                                                    placeholder="Search"
                                                    aria-label="Search"
                                                    onChange={this.getNameHotel}
                                                />
                                                <MDBBtn
                                                    gradient="aqua"
                                                    rounded
                                                    size="sm"
                                                    //type="submit"
                                                    className="mr-auto"
                                                    onClick={this.getHotelByName}

                                                >
                                                    <MDBIcon
                                                         icon="search"/>
                                                </MDBBtn>
                                                <MDBBtn gradient="aqua"
                                                        rounded
                                                        size="sm"
                                                    //type="submit"
                                                        className="mr-auto"
                                                        onClick={this.signout}><MDBIcon
                                                                                   icon="sign-out"/> Log out</MDBBtn>
                                            </FormInline>
                                            {this.state.errorSelectHotel === true ? <p>Try another hotel!</p> : null}

                                        </div>

                                    </MDBNavItem>
                                </MDBNavbarNav>

                            </MDBCollapse>
                        </MDBNavbar>

                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">

                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-5">
                            {this.state.hotel && this.state.hotel !== {} && this.state.searchHotel === true ?
                                <Col>
                                    <Card style={{width: "15rem"}}>
                                        <CardImage
                                            className="img-fluid"
                                            src={this.state.hotel.image}
                                            waves
                                        />
                                        <CardBody>
                                            <CardTitle>{this.state.hotel.name}</CardTitle>
                                            <CardText>
                                                {this.state.hotel.country},{this.state.hotel.city}
                                                <br/>
                                                {this.state.hotel.information}
                                            </CardText>
                                            <Button href="#">Offer</Button>
                                        </CardBody>
                                    </Card>
                                </Col> : (this.state.errorListHotels === true) ? <Alert bsStyle="warning">
                                    Error network connection!
                                </Alert> : <Row> {this.state.hotels.map((hotel) => {
                                    return (<Col key={hotel.idHotel}>
                                        <Card style={{width: "15rem", height: "20rem"}}>
                                            <CardImage
                                                className="img-fluid"
                                                src={hotel.image}
                                                waves
                                            />
                                            <CardBody>
                                                <CardTitle>{hotel.name}</CardTitle>

                                                <CardText>
                                                    {hotel.country},{hotel.city}
                                                    <br/>
                                                    {hotel.information}

                                                </CardText>
                                                <Button onClick={() => {
                                                    this.getOffers(hotel.idHotel, hotel.image)
                                                }}>Offer</Button>
                                            </CardBody>
                                        </Card>
                                    </Col>)
                                })}
                                </Row>
                            }

                        </MDBContainer>
                    </main>

                    <Modal isOpen={this.state.isOpenDatepicker} toggle={this.modifyIsOpenDate}>
                        <ModalHeader toggle={this.modifyIsOpenDate}>Select date</ModalHeader>
                        <ModalBody style={{width: '100%'}}>

                                {/*<MDBDatePicker cancelLabel="Effacer" locale={moment.locale('en')} getValue={this.getPickerValueStart} />*/}
                           <div>Check-in: <DatePicker
                                selected={this.state.startDate}
                                onChange={this.getPickerValueStart}
                                dateFormat={'yyyy-MM-dd'}
                            />
                           </div>
                            <br/>
                            <div>Check-out: <DatePicker
                                selected={this.state.endDate}
                                onChange={this.getPickerValueEnd}
                                dateFormat={'yyyy-MM-dd'}
                            />
                            </div>
                            <br/>

                            {this.state.errorDate===true ?
                                <Alert bsStyle="warning">
                                    Select date or select a valid (start date lower than  end date)
                                </Alert> : null}
                            <div className="text-center mt-4">
                                <MDBBtn
                                    color="light-blue"
                                    className="mb-3"
                                    type="submit"
                                    onClick={this.goToOffer}
                                >
                                    Offer
                                </MDBBtn>
                            </div>
                        </ModalBody>
                    </Modal>
                </div>


            )
        }
    }

}


export default ViewHotels;
