import React,{Component} from 'react';
import {MDBInput,MDBIcon,Row,Button, Card, CardBody, CardImage, CardTitle, CardText, Col,MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem,FormInline, MDBNavLink, MDBContainer, MDBMask, MDBView,MDBBtn } from 'mdbreact';
import {Alert} from "react-bootstrap";
//import { BrowserRouter as Router } from 'react-router-dom';

class ViewOffers  extends Component {
    constructor(props){
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
            searchHotel:false,
            offers:[],
            errorListOffers:null,
            idHotel:'',
            errorRes:null,
            numberRoom:null
        }

        let typeRoom;
        let offer;
    }


    onClickfc =() =>{
        this.setState({
            collapse: !this.state.collapse,
        });
    }
    getNumber = (event) =>{

            this.setState({numberRoom:event.target.value})

    }

    makeBook=(offer)=> {

        let obj;
        if(this.type == 'Double rooms'){
            obj = {
                "user": {
                    "id": parseInt(localStorage.getItem('userId')),
                    "firstname":localStorage.getItem('firtsname'),
                    "email": localStorage.getItem('email'),
                    "telephoneNumber": localStorage.getItem('telephoneNumber'),
                    "lastname": localStorage.getItem('name'),
                    "typeUser": 'client',
                    "password": localStorage.getItem('password'),
                    "active": localStorage.getItem('active')
                },
                "numberDoubleRoom":  parseInt(this.state.numberRoom),
                "numberTripleRoom":0,
                "offer": {
                    "idOffer": offer.idOffer,
                    "price": offer.price,
                    "dateStart": offer.dateStart,
                    "dataEnd": offer.dataEnd,
                    "hotel":
                        {
                            "idHotel": offer.hotel.idHotel,
                            "name": offer.hotel.name,
                            "city": offer.hotel.city,
                            "country": offer.hotel.country,
                            "numberStars": offer.hotel.numberStars,
                            "information": offer.hotel.information,
                            "image": offer.hotel.image,
                            "numberDoubleRooms": offer.hotel.numberDoubleRooms,
                            "numberTripleRooms": offer.hotel.numberTripleRooms
                        },
                    "numberDoubleRooms": offer.numberDoubleRooms,
                    "numberTripleRooms": offer.numberTripleRooms
                }
            }}else{
            obj = {
                "user": {
                    "id": parseInt(localStorage.getItem('userId')),
                    "firstname":localStorage.getItem('firtsname'),
                    "email": localStorage.getItem('email'),
                    "telephoneNumber": localStorage.getItem('telephoneNumber'),
                    "lastname": localStorage.getItem('name'),
                    "typeUser": 'client',
                    "password": localStorage.getItem('password'),
                    "active": localStorage.getItem('active')
                },
                "numberDoubleRoom": 0,
                "numberTripleRoom": parseInt(this.state.numberRoom),
                "offer": {
                    "idOffer": offer.idOffer,
                    "price": offer.price,
                    "dateStart": offer.dateStart,
                    "dataEnd": offer.dataEnd,
                    "hotel":
                        {
                            "idHotel": offer.hotel.idHotel,
                            "name": offer.hotel.name,
                            "city": offer.hotel.city,
                            "country": offer.hotel.country,
                            "numberStars": offer.hotel.numberStars,
                            "information": offer.hotel.information,
                            "image": offer.hotel.image,
                            "numberDoubleRooms": offer.hotel.numberDoubleRooms,
                            "numberTripleRooms": offer.hotel.numberTripleRooms
                        },
                    "numberDoubleRooms": offer.numberDoubleRooms,
                    "numberTripleRooms": offer.numberTripleRooms
                }
            }

        }

        console.log(obj)
        const options = {
            mode: 'cors',
            method: 'post',
            withCredentials: true,
            crossdomain: true,
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(obj)
        }
        fetch("https://localhost:8443/booking/rezervation/insert", options)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("error");
                }
                return response.json()
            })
            .then((data) => {
                if (data !== {}) {
                    this.setState({userObject: data, errorRes: false})
                }
            })
            .catch((error) => {
                this.setState({errorRes: true})
            });
        this.getAllOffers()
    }



    getAllOffers = () => {

            fetch(`https://localhost:8443/booking/offer/offersByDateandHotel?dateStart=${localStorage.getItem('startDate')}&dateEnd=${localStorage.getItem('endDate')}&idHotel=${localStorage.getItem('idHotel')}`)
                .then(response => {
                    if (response.status >= 400) {
                        throw new Error("error");
                    }
                    return response.json()
                })
                .then((data) => {
                    if (data !== {}) {

                        this.setState({offers: data, errorListOffers:false})
                    }
                })
                .catch((error) => {
                    this.setState({errorListOffers:true})
                });
        }

    signout = () => {
        localStorage.clear();
        this.props.history.push('/')
    }



    componentWillMount() {
        this.getAllOffers();

    }


    render() {
        if (localStorage.getItem('name') === '' || localStorage.getItem('name') === null) {
            this.props.history.push("/")
            return null
        } else {

            return (

                <div>
                    <header>

                        <MDBNavbar color="bg-primary" fixed="top" dark expand="md" scrolling transparent>
                            <MDBNavbarBrand href="/">
                                <strong>Fist Page</strong>
                            </MDBNavbarBrand>
                            {!this.state.isWideEnough && <MDBNavbarToggler onClick={this.onClickfc}/>}
                            <MDBCollapse isOpen={this.state.collapse} navbar>
                                <MDBNavbarNav left>
                                    <MDBNavItem>
                                        <MDBNavLink to="/clientpage">Home</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewhotels">Hotels</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewalloffers">Offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem active>
                                        <MDBNavLink to="/viewoffers">Hotel's offers</MDBNavLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                        <MDBNavLink to="/viewallreservations">Reservations</MDBNavLink>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                                <MDBNavbarNav right>
                                    <MDBNavItem>
                                        <MDBBtn gradient="aqua"
                                                rounded
                                                size="sm"
                                                className="mr-auto"
                                                onClick={this.signout}><MDBIcon className="text-white"
                                                                           icon="sign-out"/> Log out</MDBBtn>
                                    </MDBNavItem>
                                </MDBNavbarNav>
                            </MDBCollapse>
                        </MDBNavbar>

                        <MDBView src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">

                            </MDBMask>
                        </MDBView>
                    </header>
                    <main>
                        <MDBContainer className="text-center my-5">
                            {(this.state.errorListOffers === true) ? <Alert bsStyle="warning">
                                    There are not offers!
                                </Alert> :
                                <div>
                                    <img src={localStorage.getItem('image')} style={{height: '350px', width: '60rem'}}/>
                                     <br/>
                                     <br/>
                                    {this.state.errorRes == true ?
                                        <Alert bsStyle="warning">
                                            There are not enough rooms available!
                                        </Alert> : null}
                                    {this.state.errorRes == false ? <Alert>
                                        Booked successfully</Alert> : null}
                                        <br/>
                                    <Row> {this.state.offers.map((offer) => {
                                        return (
                                            <Col>
                                                {
                                                    offer.numberDoubleRooms === 0 && offer.numberTripleRooms!== 0 ?
                                                        this.type = 'Triple rooms' : null
                                                }
                                                {
                                                    offer.numberDoubleRooms !== 0 && offer.numberTripleRooms === 0 ?
                                                        this.type = 'Double rooms' : null
                                                }
                                                {
                                                    offer.numberDoubleRooms !== 0 && offer.numberTripleRooms !== 0 ?
                                                        this.type = 'Double rooms' : null
                                                }
                                                <br/>
                                                {offer.numberDoubleRooms === 0 && offer.numberTripleRooms === 0?
                                                    null :
                                                    <Card style={{width: "15rem"}}>

                                                        <CardBody>
                                                            <CardTitle>Offer</CardTitle>
                                                            <CardText>
                                                                <h3>{offer.price}RON</h3>
                                                                <br/>
                                                                <h5>Hotel {offer.hotel.name} </h5>
                                                                <br/>
                                                                {offer.numberDoubleRooms ===0 && offer.numberTripleRooms!==0 ?
                                                                    <p> Just Triple rooms</p>: null}
                                                                {offer.numberDoubleRooms !==0 && offer.numberTripleRooms===0 ?
                                                                    <p> Just Double rooms</p>: null}
                                                                {offer.numberDoubleRooms !==0 && offer.numberTripleRooms!==0 ?
                                                                    <p> Just Double rooms</p>: null}
                                                                <form>
                                                                    <div className="grey-text">
                                                                        <MDBInput
                                                                            label={this.type}
                                                                            icon="room"
                                                                            group
                                                                            type="number"
                                                                            validate
                                                                            onChange={this.getNumber}
                                                                        />
                                                                    </div>

                                                                    <MDBBtn gradient="aqua"
                                                                            rounded
                                                                            size="sm"
                                                                            className="mr-auto"
                                                                            onClick={()=>this.makeBook(offer)}>Reserve </MDBBtn>
                                                                </form>
                                                            </CardText>

                                                        </CardBody>
                                                    </Card>}
                                            </Col>)
                                    })}

                                    </Row>
                                </div>

                            }

                        </MDBContainer>
                    </main>

                </div>

            )
        }
    }
}

export default ViewOffers;
