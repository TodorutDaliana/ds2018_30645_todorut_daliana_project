import React, {Component} from 'react';
import '../App.css';
import {
    Card,
    CardBody,
    CardImage,
    CardTitle,
    CardText,
    Row,
    Col,
    Fa,
    Grid,
    MDBIcon,
    MDBBtn,
    Modal,
    ModalBody,
    ModalHeader,
    Container,
    Snackbar,
    MDBMask,
    MDBView
} from 'mdbreact';
import "font-awesome/css/font-awesome.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import Login from "./Login";
import Registration from "./Registration";
import {Alert} from "react-bootstrap";


class Homee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenFormLogin: false,
            isOpenFormRegistration: false,
            email: '',
            password: '',
            user: {email: 'gg'},
            errorLogin: false,
            errorReg: null,
            errors: {},
            userObj: {
                id: 6,
                email: '',
                telephoneNumber: '',
                lastname: '',
                typeUser: "client",
                password: '',
                active: true,
                firstname: '',
            }
        }
        let isValidRegData = null
        let isValidLoginData = null


    }

    modifyIsOpenForm = () => {
        this.setState({isOpenFormLogin: !this.state.isOpenFormLogin})
    }

    modifyIsOpenFormReg = () => {
        this.setState({isOpenFormRegistration: !this.state.isOpenFormRegistration})
    }

    getEmail = (event) => {
        this.setState({email: event.target.value})
    }

    getPassword = (event) => {

        this.setState({password: event.target.value})
    }

    getEmailReg = (event) => {
        this.setState({
            userObj: {
                id: this.state.userObj.id,
                email: event.target.value,
                telephoneNumber: this.state.userObj.telephoneNumber,
                lastname: this.state.userObj.lastname,
                typeUser: this.state.userObj.typeUser,
                password: this.state.userObj.password,
                active: this.state.userObj.active,
                firstname: this.state.userObj.firstname
            }
        })
    }

    getPasswordReg = (event) => {
        this.setState({
            userObj: {
                id: this.state.userObj.id,
                email: this.state.userObj.email,
                telephoneNumber: this.state.userObj.telephoneNumber,
                lastname: this.state.userObj.lastname,
                typeUser: this.state.userObj.typeUser,
                password: event.target.value,
                active: this.state.userObj.active,
                firstname: this.state.userObj.firstname
            }
        })
    }

    getPhone = (event) => {
        this.setState({
            userObj: {
                id: this.state.userObj.id,
                email: this.state.userObj.email,
                telephoneNumber: event.target.value.toString(),
                lastname: this.state.userObj.lastname,
                typeUser: this.state.userObj.typeUser,
                password: this.state.userObj.password,
                active: this.state.userObj.active,
                firstname: this.state.userObj.firstname
            }
        })
    }

    getLastName = (event) => {
        this.setState({
            userObj: {
                id: this.state.userObj.id,
                email: this.state.userObj.email,
                telephoneNumber: this.state.userObj.telephoneNumber,
                lastname: event.target.value,
                typeUser: this.state.userObj.typeUser,
                password: this.state.userObj.password,
                active: this.state.userObj.active,
                firstname: this.state.userObj.firstname
            }
        })
    }

    getFirstName = (event) => {
        this.setState({
            userObj: {
                id: this.state.userObj.id,
                email: this.state.userObj.email,
                telephoneNumber: this.state.userObj.telephoneNumber,
                lastname: this.state.userObj.lastname,
                typeUser: this.state.userObj.typeUser,
                password: this.state.userObj.password,
                active: this.state.userObj.active,
                firstname: event.target.value
            }
        })
    }

    handleValidationLogin = () => {
        let formIsValid = true;
        let errors = {}


        if (!this.state.password) {
            formIsValid = false;
            errors["password"] = " Password cannot be empty";
        }


        //Email
        if (!this.state.email) {
            formIsValid = false;
            errors["email"] = " Email cannot be empty";
        }


        if (typeof this.state.email !== "undefined" && this.state.email) {
            let lastAtPos = this.state.email.lastIndexOf('@');
            let lastDotPos = this.state.email.lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Email is not valid";
            }
        }

        this.setState({errors: errors});
        this.isValidLoginData = formIsValid;
    }


    executeLogin = () => {
        this.handleValidationLogin();
        if (this.isValidLoginData === true) {
            fetch(`https://localhost:8443/booking/user/login?email=${this.state.email}&password=${this.state.password}`)
                .then(response => {
                    if (response.status >= 400) {
                        throw new Error("error");
                    }
                    return response.json()
                })
                .then((data) => {
                    if (data !== {}) {
                        this.setState({user: data, isOpenFormLogin: false, errorLogin: false})
                        if (data.typeUser === "client") {
                            localStorage.setItem('name', data.lastname);
                            localStorage.setItem('userId', data.id);
                            localStorage.setItem('email', data.email);
                            localStorage.setItem('telephoneNumber', data.telephoneNumber);
                            localStorage.setItem('firstname', data.firstname);
                            localStorage.setItem('password', data.password);
                            localStorage.setItem('active', data.active);
                            localStorage.setItem('typeUser', data.typeUser)

                            this.props.history.push('/clientpage')
                        } else if (data.typeUser === 'admin') {
                            localStorage.setItem('name', data.lastname);
                            localStorage.setItem('userId', data.id);
                            localStorage.setItem('email', data.email);
                            localStorage.setItem('telephoneNumber', data.telephoneNumber);
                            localStorage.setItem('firstname', data.firstname);
                            localStorage.setItem('password', data.password);
                            localStorage.setItem('active', data.active);
                            localStorage.setItem('typeUser', data.typeUser)

                            this.props.history.push('/adminpage')
                        }

                    }
                })
                .catch((error) => {
                    this.setState({errorLogin: true})
                });
        }
    }


    handleValidationRegister = () => {
        let formIsValid = true;
        let errors = {}


        if (!this.state.userObj.password) {
            formIsValid = false;
            errors["passwordReg"] = " Password cannot be empty";
        }

        if (!this.state.userObj.firstname) {
            formIsValid = false;
            errors["firstname"] = "First name cannot be empty";
        }

        if (!this.state.userObj.lastname) {
            formIsValid = false;
            errors["lastname"] = "Last name cannot be empty";
        }

        if (!this.state.userObj.telephoneNumber) {
            formIsValid = false;
            errors["telephoneNumber"] = "Telephone number  cannot be empty";
        }

        //Email
        if (!this.state.userObj.email) {
            formIsValid = false;
            errors["emailReg"] = " Email cannot be empty";
        }


        if (typeof this.state.userObj.email !== "undefined" && this.state.userObj.email) {
            let lastAtPos = this.state.userObj.email.lastIndexOf('@');
            let lastDotPos = this.state.userObj.email.lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.userObj.email.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.userObj.email.length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["emailReg"] = "Email is not valid";
            }
        }

        if (typeof this.state.userObj.telephoneNumber !== "undefined" && this.state.userObj.telephoneNumber) {
            if (!(this.state.userObj.telephoneNumber.match(/\d/g) && this.state.userObj.telephoneNumber.length == 10)) {
                formIsValid = false;
                errors["telephoneNumber"] = "Telephone number is not valid"
            }
        }

        this.setState({
            errors: errors
        });
        this.isValidRegData = formIsValid
    }

    executeRegister = () => {
        this.handleValidationRegister();

        if (this.isValidRegData === true) {

            const obj = {
                "email": this.state.userObj.email,
                "telephoneNumber": this.state.userObj.telephoneNumber,
                "lastname": this.state.userObj.lastname,
                "password": this.state.userObj.password,
                "active": true,
                "firstname": this.state.userObj.firstname
            }
            const options = {
                mode: 'cors',
                method: 'post',
                withCredentials: true,
                crossdomain: true,
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(obj)
            }
            fetch("https://localhost:8443/booking/user/register", options)
                .then(response => {
                    if (response.status >= 400) {
                        throw new Error("error");
                    }
                    return response.json()
                })
                .then((data) => {
                    if (data !== {}) {
                        this.setState({userObject: data, isOpenFormRegistration: false, errorReg: false})
                    }
                })
                .catch((error) => {
                    this.setState({errorReg: true})
                });
        }

        console.log('isOpne ' + this.state.isOpenFormRegistration)
    }


    render() {


        return (

            <div style={{background: 'black'}}>
                <Row>
                    <Col xs={12} style={{textAlign: 'center', height: '10%'}}>
                        <MDBView style={{height: '10%'}} src="https://mdbootstrap.com/img/Photos/Others/img%20(40).jpg">
                            <MDBMask overlay="purple-light" className="flex-center flex-column text-white text-center">

                            </MDBMask>
                        </MDBView>

                    </Col>
                </Row>
                <Row>
                    <Col xs={1}/>
                    <Col xs={10}>
                        <Card reverse className='winter-neva-gradient'>
                            {/*<CardImage cascade style={{ height: '20rem'}} src={imm} />*/}
                            <CardBody cascade className="text-center indigo-text">
                                <CardTitle>Booking</CardTitle>
                                <CardText>Booking.com is a global technology leader connecting travelers with the widest
                                    choice of incredible places to stay. With a mission to empower people to experience
                                    the world, Booking.com invests in the digital technology that helps take the
                                    friction out of travel. Booking.com connects travelers with the world’s largest
                                    selection of incredible places to stay, including everything from apartments,
                                    vacation homes, and family-run B&Bs to 5-star luxury resorts, tree houses and even
                                    igloos. The Booking.com website and mobile apps are available in 43 languages, offer
                                    over 1.5 million properties and cover more than 121,000 destinations in 229
                                    countries and territories worldwide.</CardText>
                                <MDBBtn color="primary" size="sm" onClick={this.modifyIsOpenForm}><MDBIcon
                                    icon='sign-in'> Log in</MDBIcon></MDBBtn>
                                <MDBBtn color="primary" size="sm" onClick={this.modifyIsOpenFormReg}><MDBIcon
                                    icon='fa-user-plus'> Sign up</MDBIcon></MDBBtn>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col xs={1}/>
                </Row>

                <Modal isOpen={this.state.isOpenFormLogin} toggle={this.modifyIsOpenForm}>
                    <ModalHeader toggle={this.modifyIsOpenForm}>Login</ModalHeader>
                    <ModalBody style={{width: '100%'}}><Login getEmail={this.getEmail}
                                                              getPassword={this.getPassword}
                                                              execlogin={this.executeLogin}
                                                              error={this.state.errors}/>
                        {this.state.errorLogin ?
                            <Alert bsStyle="warning">
                                Wrong email or password!
                            </Alert> : null}
                        <div className="text-center mt-4">
                            <MDBBtn
                                color="light-blue"
                                className="mb-3"
                                onClick={this.executeLogin}
                            >
                                Login
                            </MDBBtn>
                        </div>
                    </ModalBody>
                </Modal>

                <Container>
                    <Modal isOpen={this.state.isOpenFormRegistration} toggle={this.modifyIsOpenFormReg}>
                        <ModalHeader toggle={this.modifyIsOpenFormReg}>Sign up</ModalHeader>
                        <ModalBody style={{width: '100%'}}><Registration getEmailReg={this.getEmailReg}
                                                                         getPasswordReg={this.getPasswordReg}
                                                                         getPhone={this.getPhone}
                                                                         getFirstName={this.getFirstName}
                                                                         getLastName={this.getLastName}
                                                                         error={this.state.errors}/>

                            {this.state.errorReg ?
                                <Alert bsStyle="warning">
                                    This email already exists.
                                </Alert> : null}
                            <div className="text-center mt-4">
                                <MDBBtn
                                    color="light-blue"
                                    className="mb-3"
                                    type="submit"
                                    onClick={this.executeRegister}
                                >
                                    Sign in
                                </MDBBtn>
                            </div>
                        </ModalBody>
                    </Modal>
                </Container>

            </div>

        )
    }
}

export default Homee;
