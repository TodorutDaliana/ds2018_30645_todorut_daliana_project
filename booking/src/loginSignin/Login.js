import React from "react";
import Signup from './Registration'
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";
import {Alert} from "react-bootstrap";

const Login = (props) => {
    return (

            <div>
                <MDBCard>
                        <MDBCardBody>
                            <MDBCardHeader className="form-header aqua-gradient rounded">
                                <h3 className="my-3">
                                    <MDBIcon icon="lock" /> Login:
                                </h3>
                            </MDBCardHeader>
                            <form>
                                <div className="grey-text">
                                    {typeof props.error['email'] !== 'undefined'?<span style={{color: "red"}}>{props.error["email"]}</span>: null}
                                    <MDBInput
                                        label="Type your email"
                                        icon="envelope"
                                        group
                                        type="email"
                                        validate
                                        error="wrong"
                                        success="right"
                                        onChange={props.getEmail}
                                    />
                                    {props.error['password'] ?<span style={{color: "red"}}>{props.error["password"]}</span>:null}
                                    <MDBInput
                                        label="Type your password"
                                        icon="lock"
                                        group
                                        type="password"
                                        validate
                                        onChange ={props.getPassword}
                                    />

                                </div>

                            </form>
                        </MDBCardBody>
                    </MDBCard>
            </div>
    );
};
Login.defaultProps={
    error:{}
}

export default Login;
