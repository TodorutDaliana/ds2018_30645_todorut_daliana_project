import React from "react";
import Login from './Login'
import {
    Row,
    MDBCard,
    MDBCardBody,
    MDBModalFooter,
    MDBIcon,
    MDBCardHeader,
    MDBBtn,
    MDBInput
} from "mdbreact";

const Registration = (props) => {
    return (

        <div>

            <MDBCard>
                <MDBCardBody>
                    <MDBCardHeader className="form-header aqua-gradient rounded">
                        <h3 className="my-3">
                            <MDBIcon icon="fa-user-plus" /> Sign up:
                        </h3>
                    </MDBCardHeader>
                    <form>
                        <div className="grey-text">
                            {props.error['emailReg'] ?<span style={{color: "red"}}>{props.error["emailReg"]}</span>:null}
                            <MDBInput
                                label="Type your email"
                                icon="envelope"
                                group
                                type="email"
                                validate
                                error="wrong"
                                success="right"
                                onChange ={props.getEmailReg}
                            />
                            {props.error['passwordReg'] ?<span style={{color: "red"}}>{props.error["passwordReg"]}</span>:null}
                            <MDBInput
                                label="Type your password"
                                icon="lock"
                                group
                                type="password"
                                validate
                                onChange ={props.getPasswordReg}
                            />
                            {props.error['firstname'] ?<span style={{color: "red"}}>{props.error["firstname"]}</span>:null}
                            <MDBInput
                                label="Type your first name"
                                group
                                type="name"
                                validate
                                onChange ={props.getFirstName}
                            />
                            {props.error['lastname'] ?<span style={{color: "red"}}>{props.error["lastname"]}</span>:null}
                            <MDBInput
                                label="Type your last name"
                                group
                                type="name"
                                validate
                                onChange ={props.getLastName}
                            />
                            {props.error['telephoneNumber'] ?<span style={{color: "red"}}>{props.error["telephoneNumber"]}</span>:null}
                            <MDBInput
                                label="Type your phone number"
                                icon='phone'
                                group
                                type="phone number"
                                validate
                                error="wrong"
                                success="right"
                                onChange ={props.getPhone}
                            />
                        </div>


                    </form>
                </MDBCardBody>
            </MDBCard>
        </div>
    );
};

export default Registration;