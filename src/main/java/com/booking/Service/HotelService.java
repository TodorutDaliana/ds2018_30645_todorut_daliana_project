package com.booking.Service;

import com.booking.business.dto.HotelDTO;
import com.booking.dataAccess.model.Hotel;
import com.booking.dataAccess.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.booking.util.ExceptionBusiness;
import com.booking.util.ExceptionHandle;
import com.booking.util.HotelTransformation;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HotelService {

    @Autowired
    private HotelRepository hotelRepository;
    private HotelTransformation hotelTransformation;

    public HotelDTO insert(HotelDTO hotelDTO) throws ExceptionBusiness {

        List<Hotel> hotels = hotelRepository.findByName(hotelDTO.getName());
        if(!hotels.isEmpty()){
            throw new ExceptionBusiness(ExceptionHandle.HOTEL_NAME_EXIST_ALREADY);
        }

        if (hotelDTO.getInformation() == null  || hotelDTO.getCity() == null || hotelDTO.getCountry() == null || hotelDTO.getName() == null || hotelDTO.getNumberStars() == 0) {
            throw new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }

        Hotel hotel = hotelTransformation.toHotel(hotelDTO);
        hotelRepository.save(hotel);
        return hotelTransformation.toDto(hotel);
    }

    public HotelDTO delete(int id) throws ExceptionBusiness {
      if((Integer) id == null) {
          throw new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
      }

      Optional<Hotel> hotels = hotelRepository.findById(id);
      if(!hotels.isPresent()){
          throw new ExceptionBusiness(ExceptionHandle.NAME_HOTEL_NOT_VALID);
      }else{
          hotelRepository.delete(hotels.get());
          HotelDTO hotelDTO = hotelTransformation.toDto(hotels.get());
          return  hotelDTO;
      }
    }

    public HotelDTO selectHotel(String name) throws ExceptionBusiness {
        if(name == null){
            throw  new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }

        List<Hotel> hotels = hotelRepository.findByName(name);
        if(hotels.isEmpty()){
            throw new ExceptionBusiness(ExceptionHandle.NAME_HOTEL_NOT_VALID);
        }else{
            HotelDTO hotelDTO = hotelTransformation.toDto(hotels.get(0));
            return hotelDTO;
        }
    }

    public List<HotelDTO> listAllHotels(){
        List<HotelDTO> listHotels = new ArrayList<>();
        List<Hotel> findAllHotels = hotelRepository.findAll();
        for(Hotel hotel:findAllHotels){
            listHotels.add(hotelTransformation.toDto(hotel));

        }
        return listHotels;
    }

    public HotelDTO edit(HotelDTO hotelDTO) throws ExceptionBusiness {
        Optional<Hotel> hotel = hotelRepository.findById(hotelDTO.getIdHotel());
        if(!hotel.isPresent()){
            throw new ExceptionBusiness(ExceptionHandle.ID_INVALID);
        } else {
            Hotel hotel1 = hotel.get();
            hotel1.setNumberDoubleRooms(hotelDTO.getNumberDoubleRooms());
            hotel1.setNumberTripleRooms(hotelDTO.getNumberTripleRooms());
            hotel1.setNumberStars(hotelDTO.getNumberStars());
            hotel1.setImage(hotelDTO.getImage());
            hotel1.setInformation(hotelDTO.getInformation());
            hotel1.setCity(hotelDTO.getCity());
            hotel1.setCountry(hotelDTO.getCountry());
            hotel1.setName(hotelDTO.getName());
            hotelRepository.save(hotel1);
            return hotelTransformation.toDto(hotel1);
        }
    }





}
