package com.booking.Service;

import com.booking.business.dto.OfferDTO;
import com.booking.dataAccess.model.Hotel;
import com.booking.dataAccess.model.Offer;
import com.booking.dataAccess.repository.HotelRepository;
import com.booking.dataAccess.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.booking.util.ExceptionBusiness;
import com.booking.util.ExceptionHandle;
import com.booking.util.OfferTransformation;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OfferService {

    @Autowired
    private OfferRepository offerRepository;
    private OfferTransformation offerTransformation;
    @Autowired
    private HotelRepository hotelRepository;

    public OfferDTO insert(OfferDTO offerDTO) throws ExceptionBusiness {
        Optional<Hotel>  hotel = hotelRepository.findById(offerDTO.getHotel().getIdHotel());
        if(!hotel.isPresent()){
            throw new ExceptionBusiness(ExceptionHandle.ID_INVALID);
        }
        if(offerDTO.getHotel()== null || offerDTO.getDataEnd() == null || offerDTO.getDateStart() == null || (Integer)offerDTO.getIdOffer() == null || (Integer)offerDTO.getNumberDoubleRooms()==null || (Integer)offerDTO.getNumberTripleRooms() == null){
            throw new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }
        offerDTO.setNumberTripleRooms(hotel.get().getNumberTripleRooms());
        offerDTO.setNumberDoubleRooms(hotel.get().getNumberDoubleRooms());
        Offer offer = offerTransformation.toOffer(offerDTO);
        offerRepository.save(offer);
        return offerTransformation.toDto(offer);
    }

    public OfferDTO delete(int id) throws ExceptionBusiness{
        if((Integer)id == null){
            throw new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }
        Optional<Offer> offer = offerRepository.findById(id);
        if(!offer.isPresent()){
            throw new ExceptionBusiness(ExceptionHandle.ID_INVALID);
        }else {
            offerRepository.deleteById(id);
            return offerTransformation.toDto(offer.get());
        }
    }

    public List<OfferDTO> findAll() {
        List<OfferDTO> listOffers = new ArrayList<>();
        List<Offer> findAllOffer = offerRepository.findAll();
        for(Offer offer: findAllOffer){
            listOffers.add(offerTransformation.toDto(offer));
        }
        return listOffers;
    }

    public OfferDTO edit( OfferDTO offer) throws  ExceptionBusiness{
        Optional<Offer> offer1 = offerRepository.findById(offer.getIdOffer());
        if(!offer1.isPresent()){
            throw new ExceptionBusiness(ExceptionHandle.ID_INVALID);
        }else{
            Optional<Hotel> hotel = hotelRepository.findById(offer.getHotel().getIdHotel());
            Offer offer2 = offer1.get();
            offer2.setPrice(offer.getPrice());
            offer2.setNumberTripleRooms(offer.getNumberTripleRooms());
            offer2.setNumberDoubleRooms(offer.getNumberDoubleRooms());
            offer2.setHotel(hotel.get());
            offer2.setDateStart(offer.getDateStart());
            offer2.setDataEnd(offer.getDataEnd());
            offerRepository.save(offer2);
            return offerTransformation.toDto(offer2);
        }
    }


    public List<OfferDTO> findByHotel(int id) throws  ExceptionBusiness{
        Optional<Hotel> hotel1 = hotelRepository.findById(id);
        if(!hotel1.isPresent()){
            throw new ExceptionBusiness(ExceptionHandle.ID_INVALID);
        }else{
            List<OfferDTO> offerDTOS = new ArrayList<>();
            List<Offer> findByHotel = offerRepository.findByHotel(hotel1.get());
            for(Offer offer:findByHotel){
                offerDTOS.add(offerTransformation.toDto(offer));
            }
            return offerDTOS;
        }
    }

    public List<OfferDTO> findByDateStartAndEnd(Date start, Date end) throws ExceptionBusiness {
        if(start==null || end==null){
            throw  new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }else{
            List<OfferDTO> offerDTOS = new ArrayList<>();
            List<Offer> findByDateStartAndEnd = offerRepository.findByDateStartAndDateEnd(start,end);
            for(Offer offer:findByDateStartAndEnd){
                offerDTOS.add(offerTransformation.toDto(offer));
            }
            return offerDTOS;
        }
    }

    public List<OfferDTO> findByDateStartEndAndIdHotel(Date start, Date end,int idHotel) throws ExceptionBusiness {
        Optional<Hotel> hotel = hotelRepository.findById(idHotel);
        if(start==null || end==null || !hotel.isPresent()){
            throw  new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }else{
            List<OfferDTO> offerDTOS = new ArrayList<>();
            List<Offer> findByDateStartAndEnd = offerRepository.findByDateStartAndDateEndandIdHotel(start,end,hotel.get());
            for(Offer offer:findByDateStartAndEnd){
                offerDTOS.add(offerTransformation.toDto(offer));
            }
            return offerDTOS;
        }
    }

    public List<OfferDTO> findByDateStartEndAndCity(Date start, Date end, String city) throws ExceptionBusiness {
        List<Hotel> hotels = hotelRepository.findByCity(city);
        if(start==null || end==null || hotels == null){
            throw  new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }else{
            List<OfferDTO> offerDTOS = new ArrayList<>();
            List<Offer> findByDateStartAndEndHotel = new ArrayList<>();
            for(Hotel hotel:hotels){
                findByDateStartAndEndHotel.addAll( offerRepository.findByDateStartAndDateEndandIdHotel(start,end,hotel));
            }

            for(Offer offer:findByDateStartAndEndHotel){
                offerDTOS.add(offerTransformation.toDto(offer));
            }
            return offerDTOS;
        }
    }




}
