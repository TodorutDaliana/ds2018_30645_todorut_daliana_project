package com.booking.Service;

import com.booking.business.dto.RezervationDTO;
import com.booking.dataAccess.model.Offer;
import com.booking.dataAccess.model.Rezervation;
import com.booking.dataAccess.model.User;
import com.booking.dataAccess.repository.OfferRepository;
import com.booking.dataAccess.repository.RezervationRepository;
import com.booking.dataAccess.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.booking.util.ExceptionBusiness;
import com.booking.util.ExceptionHandle;
import com.booking.util.OfferTransformation;
import com.booking.util.RezervationTransformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RezervationService {

    @Autowired
    private RezervationRepository rezervationRep;
    private RezervationTransformation rezervationTransformation;
    private OfferTransformation offerTransformation;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailService emailService;

    public RezervationDTO insert(RezervationDTO rezervationDTO) throws ExceptionBusiness{
        Optional<Offer> offer = offerRepository.findById(rezervationDTO.getOffers().getIdOffer());
        Optional<User> user = userRepository.findById(rezervationDTO.getUser().getId());
//        if(!offer.isPresent() || !user.isPresent()){
//            throw  new ExceptionBusiness(ExceptionHandle.ID_INVALID);
//        }
        if(offer.get().getNumberDoubleRooms()<rezervationDTO.getNumberDoubleRoom() || offer.get().getNumberTripleRooms()<rezervationDTO.getNumberTripleRoom() ){
            throw new ExceptionBusiness(ExceptionHandle.ROOMS_NOT_AVAILABLE);
        }
        offer.get().setNumberDoubleRooms(offer.get().getNumberDoubleRooms()-rezervationDTO.getNumberDoubleRoom());
        offer.get().setNumberTripleRooms(offer.get().getNumberTripleRooms()-rezervationDTO.getNumberTripleRoom());
        rezervationDTO.setOffer(offer.get());
        rezervationDTO.setUser(user.get());
        Rezervation rezervation = rezervationTransformation.toRezervation(rezervationDTO);
        offerRepository.save(offer.get());
        rezervationRep.save(rezervation);
        emailService.sendMail(user.get().getEmail(),"Rezervation",rezervation.toString());
        return rezervationDTO;
    }

    public List<RezervationDTO> findByUser(String email) throws ExceptionBusiness {
        if(email == null){
            throw  new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }


        List<User> user = userRepository.findByEmail(email);
        if(user.isEmpty()){
            throw new ExceptionBusiness(ExceptionHandle.EMAIL_NOT_VALID);
        }else {
            List<RezervationDTO> rezervationDTOS = new ArrayList<>();
            List<Rezervation> findByUser = rezervationRep.findByUser(user.get(0));
            for(Rezervation rezervation:findByUser){
                rezervationDTOS.add(rezervationTransformation.toDto(rezervation));
            }
            return rezervationDTOS;
        }
    }


}
