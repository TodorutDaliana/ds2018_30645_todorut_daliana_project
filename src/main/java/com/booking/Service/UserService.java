package com.booking.Service;

import com.booking.business.dto.UserDTO;
import com.booking.dataAccess.model.User;
import com.booking.dataAccess.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.booking.util.ExceptionBusiness;
import com.booking.util.ExceptionHandle;
import com.booking.util.UserTransformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private UserRepository userRep;
    private UserTransformation userTransformation;



    public UserDTO register(UserDTO userDto) throws ExceptionBusiness {
        validateRegistration(userDto);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        User user = userTransformation.toUser(userDto);
        user.setTypeUser("client");
        String pass = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(pass);

        user.setActive(true);
        userRep.save(user);
        return userTransformation.toDto(user);
    }


    public List<UserDTO> listAllUser(){
        List<UserDTO> listUsers = new ArrayList<>();
        List<User> findAll = userRep.findAll();
        for(User user:findAll){
            listUsers.add(userTransformation.toDto(user));
        }

        return listUsers;
    }

    public  UserDTO login(String email, String password) throws ExceptionBusiness {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        validateLogin(password,email);
       List<User> user = userRep.findByEmail(email);
       if(user.isEmpty()){
           throw new ExceptionBusiness(ExceptionHandle.EMAIL_NOT_VALID);
       }

       if(!bCryptPasswordEncoder.matches(password,user.get(0).getPassword())){
           throw new ExceptionBusiness(ExceptionHandle.PASSWORD_NOT_VALID);
       }

       UserDTO userDTO = userTransformation.toDto(user.get(0));
       return  userDTO;

    }


    public UserDTO deactivate(String email) throws ExceptionBusiness {
        List<User> user = userRep.findByEmail(email);
        if(user.isEmpty()){
            throw new ExceptionBusiness(ExceptionHandle.EMAIL_NOT_VALID);
        } else {
            User user1 = user.get(0);
            user1.setActive(!user1.isActive());
            User userUpd = userRep.save(user1);
            return userTransformation.toDto(userUpd);
        }
    }


    public UserDTO edit(UserDTO userDTO) throws ExceptionBusiness {
        Optional<User> user = userRep.findById(userDTO.getId());
        if(!user.isPresent()){
            throw new ExceptionBusiness(ExceptionHandle.ID_INVALID);
        } else {
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            User user1 = user.get();
            user1.setTelephoneNumber(userDTO.getTelephoneNumber());
            user1.setLastname(userDTO.getLastname());
            user1.setFirstname(userDTO.getFirstname());
            user1.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
            user1.setEmail(userDTO.getEmail());
            user1.setTypeUser(userDTO.getTypeUser());
            User userUpd = userRep.save(user1);
            return userTransformation.toDto(userUpd);
        }
    }

    private void validateRegistration(UserDTO user) throws ExceptionBusiness{

        if( user.getEmail() == null || user.getFirstname() == null || user.getPassword() == null || user.getLastname() == null || user.getTelephoneNumber() == null){
            throw new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }
        List<User> userList = userRep.findByEmail(user.getEmail());
        if(!userList.isEmpty()){
            throw  new ExceptionBusiness(ExceptionHandle.EMAIL_EXIST_ALREADY);
        }

        final Pattern validEmailAddressRegex =Pattern.compile("^(\\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\\s|\\.|\\-)?([0-9]{3}(\\s|\\.|\\-|)){2}$",Pattern.CASE_INSENSITIVE);
        Matcher matcher = validEmailAddressRegex.matcher(user.getTelephoneNumber());
        if(matcher.find() == false){
            throw new ExceptionBusiness(ExceptionHandle.NUMBER_NOT_VALID);
        }

        final Pattern validEmailAddress =
                Pattern.compile("^[A-Z0-9._%+-]+@gmail.com$", Pattern.CASE_INSENSITIVE);

        Matcher matcherEmail = validEmailAddress.matcher(user.getEmail());
        if(matcherEmail.find() == false){
            throw new ExceptionBusiness(ExceptionHandle.EMAIL_NOT_VALID);
        }
    }

    private void validateLogin(String password, String email) throws ExceptionBusiness{
        if(password == "" ||  password == null || email == "" || email == null ){
            throw new ExceptionBusiness(ExceptionHandle.NULL_INPUT);
        }

        final Pattern validEmailAddress = Pattern.compile("^[A-Z0-9._%+-]+@gmail.com$", Pattern.CASE_INSENSITIVE);

        Matcher matcherEmail = validEmailAddress.matcher(email);
        if(matcherEmail.find() == false){
            throw new ExceptionBusiness(ExceptionHandle.EMAIL_NOT_VALID);
        }
    }


//    @Override
//    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
//
//        User user =  userRep.findByEmail(s).get(0);
//        CustomUserDetails userDetails;
//        if(user !=null){
//            userDetails = new CustomUserDetails();
//            userDetails.setUser(user);
//
//    }else{
//        throw new UsernameNotFoundException("user not exist");
//    }
//    return userDetails;
//
//    }
}
