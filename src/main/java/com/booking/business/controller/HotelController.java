package com.booking.business.controller;


import com.booking.Service.HotelService;
import com.booking.business.dto.HotelDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.booking.util.ExceptionBusiness;

import java.util.List;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/booking/hotel")
public class HotelController {

    @Autowired
    HotelService hotelService;

    @PostMapping(value = "/insert")
    public ResponseEntity<?> insertHotel(@RequestBody HotelDTO hotelDTO){
        try{
            HotelDTO hotel = hotelService.insert(hotelDTO);
            return new ResponseEntity<>(hotel, HttpStatus.OK);
        } catch (ExceptionBusiness exceptionBusiness) {
            return new ResponseEntity<>(exceptionBusiness,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/listHotel",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HotelDTO> getAll(){
        List<HotelDTO> hotelDTOList = hotelService.listAllHotels();
        return  hotelDTOList;
    }


    @PostMapping(value = "/delete")
    public ResponseEntity<?> deleteHotel(@RequestParam("id") int id){
        try{
            HotelDTO hotelDTO = hotelService.delete(id);
            return new ResponseEntity<>(hotelDTO,HttpStatus.OK);
        }catch (ExceptionBusiness b){
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/edit")
    public ResponseEntity<?> editHotel(@RequestBody HotelDTO hotelDTO) {
        try {
            HotelDTO hotelDTO1 = hotelService.edit(hotelDTO);
            return new ResponseEntity<>(hotelDTO1, HttpStatus.OK);
        } catch (ExceptionBusiness b) {
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/findByName")
    public ResponseEntity<?> findHotel(@RequestParam("name") String name){
        try{
            HotelDTO hotelDTO = hotelService.selectHotel(name);
            return new ResponseEntity<>(hotelDTO,HttpStatus.OK);
        } catch (ExceptionBusiness b) {
            return new ResponseEntity<>(b,HttpStatus.BAD_REQUEST);
        }
    }

}
