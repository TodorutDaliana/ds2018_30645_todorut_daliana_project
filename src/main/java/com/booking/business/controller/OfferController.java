package com.booking.business.controller;

import com.booking.Service.OfferService;
import com.booking.business.dto.OfferDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.booking.util.ExceptionBusiness;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/booking/offer")
public class OfferController {

    @Autowired
    OfferService offerService;

    @PostMapping(value = "/insert")
    public ResponseEntity<?> insertOffer(@RequestBody OfferDTO offerDTO){
        try{
            OfferDTO  offerDTO1 = offerService.insert(offerDTO);
            return new ResponseEntity<>(offerDTO1, HttpStatus.OK);
        } catch (ExceptionBusiness b){
            return new ResponseEntity<>(b,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/allOffers",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OfferDTO> getAllOffers(){
        List<OfferDTO> listOffers = offerService.findAll();
        return listOffers;
    }

    @PostMapping(value = "/deleteOffer")
    public ResponseEntity<?> deleteOffer(@RequestParam("id") int id){
        try{
            OfferDTO offerDTO1 = offerService.delete(id);
            return new ResponseEntity<>(offerDTO1,HttpStatus.OK);
        }catch (ExceptionBusiness b){
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/updateOffer")
    public ResponseEntity<?> deleteOffer(@RequestBody OfferDTO offerDTO){
        try{
            OfferDTO offerDTO1 = offerService.edit(offerDTO);
            return new ResponseEntity<>(offerDTO1,HttpStatus.OK);
        }catch (ExceptionBusiness b){
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping(value = "/offers",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OfferDTO> getOffersByHotel(@RequestParam("id") int id) throws ExceptionBusiness {
        List<OfferDTO> listOffers = offerService.findByHotel(id);
        return listOffers;
    }

    @GetMapping(value = "/offersByDate",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OfferDTO> getOffersByDate(@RequestParam("dateStart") String start, @RequestParam("dateEnd") String end) throws ExceptionBusiness, ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed =  format.parse(start);
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        Date parsed1 =  format.parse(end);
        java.sql.Date sql1 = new java.sql.Date(parsed1.getTime());

        List<OfferDTO> listOffers = offerService.findByDateStartAndEnd(sql,sql1);
        return listOffers;
    }

    @GetMapping(value = "/offersByDateandHotel",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OfferDTO> getOffersByDateAndHotel(@RequestParam("dateStart") String start, @RequestParam("dateEnd") String end, @RequestParam("idHotel") int id) throws ExceptionBusiness, ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed =  format.parse(start);
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        Date parsed1 =  format.parse(end);
        java.sql.Date sql1 = new java.sql.Date(parsed1.getTime());

        List<OfferDTO> listOffers = offerService.findByDateStartEndAndIdHotel(sql,sql1,id);
        return listOffers;
    }

    @GetMapping(value = "/offersByDateandCity",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OfferDTO> getOffersByDateAndCity(@RequestParam("dateStart") String start, @RequestParam("dateEnd") String end, @RequestParam("city") String city) throws ExceptionBusiness, ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed =  format.parse(start);
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        Date parsed1 =  format.parse(end);
        java.sql.Date sql1 = new java.sql.Date(parsed1.getTime());

        List<OfferDTO> listOffers = offerService.findByDateStartEndAndCity(sql,sql1,city);
        return listOffers;
    }


}
