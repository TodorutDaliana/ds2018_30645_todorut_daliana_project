package com.booking.business.controller;

import com.booking.Service.RezervationService;
import com.booking.business.dto.RezervationDTO;
import com.booking.util.ExceptionBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/booking/rezervation")
public class RezervationController {

    @Autowired
    RezervationService rezervationService;

    @PostMapping(value = "/insert")
    public ResponseEntity<?> insertRezervation(@RequestBody RezervationDTO rezervationDTO){
        try{
            RezervationDTO rezervationDTO1 = rezervationService.insert(rezervationDTO);
            return new ResponseEntity<>(rezervationDTO1, HttpStatus.OK);
        } catch (ExceptionBusiness exceptionBusiness) {
            return new ResponseEntity<>(exceptionBusiness,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/allRezervationUser",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RezervationDTO> getRezervation(@RequestParam("email") String email) throws ExceptionBusiness {
                List<RezervationDTO> rezervationDTOList = rezervationService.findByUser(email);
                return  rezervationDTOList;
    }
}
