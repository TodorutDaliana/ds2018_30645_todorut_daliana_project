package com.booking.business.controller;


import com.booking.Service.UserService;
import com.booking.business.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.booking.util.ExceptionBusiness;

import java.util.List;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/booking/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/register")
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userService.register(userDTO);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (ExceptionBusiness exceptionBusiness) {
            return new ResponseEntity<>(exceptionBusiness, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/allUsers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDTO> getAllUsers() {

        List<UserDTO> listUser = userService.listAllUser();
        return listUser;

    }

    @GetMapping(value = "/login",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@RequestParam("email") String email, @RequestParam("password") String password) {
        try {
            UserDTO userDTO = userService.login(email, password);
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (ExceptionBusiness b) {
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/deactivateUser")
    public ResponseEntity<?> deactivateUser(@RequestParam("email") String email){
        try {
            UserDTO userDTO = userService.deactivate(email);
            return new ResponseEntity<>(userDTO,HttpStatus.OK);
        } catch (ExceptionBusiness b) {
            return new ResponseEntity<>(b, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/editUser")
    public ResponseEntity<?> editUser(@RequestBody UserDTO userDTO){
        try {
            UserDTO userDTO1= userService.edit(userDTO);
            return  new ResponseEntity<>(userDTO1,HttpStatus.OK);
        } catch (ExceptionBusiness b){
            return new ResponseEntity<>(b,HttpStatus.BAD_REQUEST);
        }
    }
}
