package com.booking.business.dto;

public class HotelDTO {

    private int idHotel;
    private String name;
    private String city;
    private String country;
    private int numberStars;
    private String information;
    private String image;
    private int numberDoubleRooms;
    private int numberTripleRooms;


    public int getNumberDoubleRooms() {
        return numberDoubleRooms;
    }

    public void setNumberDoubleRooms(int numberDoubleRooms) {
        this.numberDoubleRooms = numberDoubleRooms;
    }

    public int getNumberTripleRooms() {
        return numberTripleRooms;
    }

    public void setNumberTripleRooms(int numberTripleRooms) {
        this.numberTripleRooms = numberTripleRooms;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumberStars() {
        return numberStars;
    }

    public void setNumberStars(int numberStars) {
        this.numberStars = numberStars;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String  getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
