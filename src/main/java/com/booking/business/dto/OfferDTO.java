package com.booking.business.dto;
import com.booking.dataAccess.model.Hotel;

import java.sql.Date;

public class OfferDTO {

    private int idOffer;
    private double price;
    private Date dateStart;
    private Date dataEnd;
    private Hotel hotel;
    private int numberDoubleRooms;
    private int numberTripleRooms;

    public int getIdOffer() {
        return idOffer;
    }

    public void setIdOffer(int idOffer) {
        this.idOffer = idOffer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(Date dataEnd) {
        this.dataEnd = dataEnd;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public int getNumberDoubleRooms() {
        return numberDoubleRooms;
    }

    public void setNumberDoubleRooms(int numberDoubleRooms) {
        this.numberDoubleRooms = numberDoubleRooms;
    }

    public int getNumberTripleRooms() {
        return numberTripleRooms;
    }

    public void setNumberTripleRooms(int numberTripleRooms) {
        this.numberTripleRooms = numberTripleRooms;
    }
}
