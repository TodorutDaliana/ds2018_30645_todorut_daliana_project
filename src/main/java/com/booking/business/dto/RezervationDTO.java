package com.booking.business.dto;
import com.booking.dataAccess.model.Offer;
import com.booking.dataAccess.model.User;

public class RezervationDTO {


    private int idRezervation;
    private Offer offer;
    private User user;
    private int numberDoubleRoom;
    private int numberTripleRoom;

    public int getNumberDoubleRoom() {
        return numberDoubleRoom;
    }

    public void setNumerDoubleRoom(int numberDoubleRoom) {
        this.numberDoubleRoom = numberDoubleRoom;
    }

    public int getNumberTripleRoom() {
        return numberTripleRoom;
    }

    public void setNumberTripleRoom(int numberTripleRoom) {
        this.numberTripleRoom = numberTripleRoom;
    }

    public int getIdRezervation() {
        return idRezervation;
    }

    public void setIdRezervation(int idRezervation) {
        this.idRezervation = idRezervation;
    }

    public Offer getOffers() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
