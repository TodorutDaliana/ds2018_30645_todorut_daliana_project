package com.booking.business.dto;

public class UserDTO {


    private int id;
    private String firstname;
    private String email;
    private String telephoneNumber;
    private String lastname;
    private String typeUser;
    private String password;
    private boolean active;

    public String getLastname() {
        return lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public int getId() {
        return id;
    }

    public boolean isActive() {
        return active;
    }


    public String getFirstname() {
        return firstname;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }


    public void setActive(boolean active) {
        this.active = active;
    }
}