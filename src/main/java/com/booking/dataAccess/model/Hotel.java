package com.booking.dataAccess.model;


import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Hotel {

    @Id
    private int idHotel;
    private String name;
    private String city;
    private String country;
    private int numberStars;
    private String information;
    private String image;
    private int numberDoubleRooms;
    private int numberTripleRooms;


    public Hotel(){
    }

//    public Hotel(int idHotel, String name, String city, String country, int numberStars, String information, String image, int numberDoubleRooms, int numberTripleRooms) {
//        this.idHotel = idHotel;
//        this.name = name;
//        this.city = city;
//        this.country = country;
//        this.numberStars = numberStars;
//        this.information = information;
//        this.image = image;
//        this.numberDoubleRooms = numberDoubleRooms;
//        this.numberTripleRooms = numberTripleRooms;
//    }


    public int getNumberDoubleRooms() {
        return numberDoubleRooms;
    }

    public void setNumberDoubleRooms(int numberDoubleRooms) {
        this.numberDoubleRooms = numberDoubleRooms;
    }

    public int getNumberTripleRooms() {
        return numberTripleRooms;
    }

    public void setNumberTripleRooms(int numberTripleRooms) {
        this.numberTripleRooms = numberTripleRooms;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumberStars() {
        return numberStars;
    }

    public void setNumberStars(int numberStars) {
        this.numberStars = numberStars;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String  getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return System.lineSeparator()+"Hotel details" +System.lineSeparator()+
                "name:" + name + System.lineSeparator() +
                "city:" + city + System.lineSeparator() +
                "country:" + country + System.lineSeparator() +
                " numberStars:" + numberStars + System.lineSeparator()+
                "information:" + information + System.lineSeparator() +
                " image:" + image;
    }
}
