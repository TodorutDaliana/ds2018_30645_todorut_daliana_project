package com.booking.dataAccess.model;



import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Date;

@Entity
public class Offer {

    @Id
    private int idOffer;
    private double price;
    private Date dateStart;
    private Date dataEnd;
    @ManyToOne
    @JoinColumn(name="hotel_id")
    private Hotel hotel;
    private int numberDoubleRooms;
    private int numberTripleRooms;


    public Offer() {
    }

//    public Offer(int idOffer, double price, Date dateStart, Date dataEnd, Hotel hotel, int numberDoubleRooms, int numberTripleRooms) {
//        this.idOffer = idOffer;
//        this.price = price;
//        this.dateStart = dateStart;
//        this.dataEnd = dataEnd;
//        this.hotel = hotel;
//        this.numberDoubleRooms = numberDoubleRooms;
//        this.numberTripleRooms = numberTripleRooms;
//    }

    public int getIdOffer() {
        return idOffer;
    }

    public void setIdOffer(int idOffer) {
        this.idOffer = idOffer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(Date dataEnd) {
        this.dataEnd = dataEnd;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public int getNumberDoubleRooms() {
        return numberDoubleRooms;
    }

    public void setNumberDoubleRooms(int numberDoubleRooms) {
        this.numberDoubleRooms = numberDoubleRooms;
    }

    public int getNumberTripleRooms() {
        return numberTripleRooms;
    }

    public void setNumberTripleRooms(int numberTripleRooms) {
        this.numberTripleRooms = numberTripleRooms;
    }


    @Override
    public String toString() {
        return "Offer details" +
                "price:" + price + System.lineSeparator()+
                "dateStart:" + dateStart +System.lineSeparator()+
                "dataEnd:" + dataEnd + System.lineSeparator()+
                "hotel:" + hotel.toString() + System.lineSeparator()+
                "numberDoubleRooms:" + numberDoubleRooms + System.lineSeparator()+
                "numberTripleRooms:" + numberTripleRooms;
    }
}
