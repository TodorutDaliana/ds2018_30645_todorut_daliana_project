package com.booking.dataAccess.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Rezervation {

    @Id
    private int idRezervation;
    @ManyToOne
    @JoinColumn(name="id_offer")
    private Offer offer;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    private int numberDoubleRoom;
    private int numberTripleRoom;

//    public Rezervation(int idRezervation, Offer offer, User user,int numberDoubleRoom, int numberTripleRoom) {
//        this.idRezervation = idRezervation;
//        this.offer = offer;
//        this.user = user;
//        this.numberDoubleRoom = numberDoubleRoom;
//        this.numberTripleRoom = numberTripleRoom;
//    }

    public Rezervation() {
    }

    public int getNumberDoubleRoom() {
        return numberDoubleRoom;
    }

    public void setNumberDoubleRoom(int numberDoubleRoom) {
        this.numberDoubleRoom = numberDoubleRoom;
    }

    public int getNumberTripleRoom() {
        return numberTripleRoom;
    }

    public void setNumberTripleRoom(int numberTripleRoom) {
        this.numberTripleRoom = numberTripleRoom;
    }

    public int getIdRezervation() {
        return idRezervation;
    }

    public void setIdRezervation(int idRezervation) {
        this.idRezervation = idRezervation;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return System.lineSeparator()+"Rezervation detalies" +System.lineSeparator()+
                "offer:" + offer.toString() +System.lineSeparator()+
                " user:" + user.toString() +System.lineSeparator()+
                "numberDoubleRoom:" + numberDoubleRoom +System.lineSeparator()+
                "numberTripleRoom:" + numberTripleRoom;
    }
}
