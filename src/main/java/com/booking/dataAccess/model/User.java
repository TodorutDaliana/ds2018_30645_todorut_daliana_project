package com.booking.dataAccess.model;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class User {

    @Id
    private int id;
    private String lastname;
    private String firstname;
    private String email;
    private String telephoneNumber;
    private String typeUser;
    private String password;
    private boolean active;


    public User() {
    }

    public User(int id, String lastname,String firstname,String password, String email, String telephoneNumber,boolean active) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.telephoneNumber = telephoneNumber;
        this.password = password;
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public int getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public boolean isActive() {
        return active;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "User details" + System.lineSeparator()+
                "last name:'" + lastname + System.lineSeparator() +
                " first name:" + firstname + System.lineSeparator() +
                " email:" + email + System.lineSeparator() +
                "telephoneNumber:" + telephoneNumber;
    }
}