package com.booking.dataAccess.repository;

import com.booking.dataAccess.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HotelRepository extends JpaRepository<Hotel, Integer> {

    List<Hotel> findByName(String name);
    List<Hotel> findByCity(String name);
}

