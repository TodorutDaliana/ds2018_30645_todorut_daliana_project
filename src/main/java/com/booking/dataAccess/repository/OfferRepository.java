package com.booking.dataAccess.repository;
import com.booking.dataAccess.model.Hotel;
import com.booking.dataAccess.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface OfferRepository extends JpaRepository<Offer, Integer> {
List<Offer> findByHotel(Hotel hotel);
@Query("SELECT o FROM Offer o where o.dataEnd=:dataEnd and o.dateStart=:dataStart")
List<Offer> findByDateStartAndDateEnd(@Param("dataStart") Date start,@Param("dataEnd") Date end);

@Query("SELECT o FROM Offer o where o.dataEnd=:dataEnd and o.dateStart=:dataStart and o.hotel=:hotel")
List<Offer> findByDateStartAndDateEndandIdHotel(@Param("dataStart") Date start, @Param("dataEnd") Date end, @Param("hotel") Hotel hotel);

}

