package com.booking.dataAccess.repository;
import com.booking.dataAccess.model.Rezervation;
import com.booking.dataAccess.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface RezervationRepository extends JpaRepository<Rezervation, Integer> {
    List<Rezervation> findByUser(User user);

}

