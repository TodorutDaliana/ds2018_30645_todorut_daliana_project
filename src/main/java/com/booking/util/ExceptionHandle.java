package com.booking.util;

public enum ExceptionHandle {


    UNKNOWN_EXCEPTION("Unknown"),
    NULL_INPUT("Validation Exception"),
    ENCRYPT_FAIL("password encryption failed"),
    PASSWORD_NOT_VALID("Password not valid."),
    EMAIL_NOT_VALID("Email not valid"),
    USERNAME_NOT_VALID("Username not valid"),
    NAME_HOTEL_NOT_VALID("Name's hotel not valid"),
    EMAIL_EXIST_ALREADY("Username already exists Exception"),
    HOTEL_NAME_EXIST_ALREADY("Name's hotel already exists"),
    NUMBER_NOT_VALID("Number not valid"),
    ROOMS_NOT_AVAILABLE("Rooms not available"),
    ID_INVALID("Id is invalid");
    String message;

    ExceptionHandle(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
