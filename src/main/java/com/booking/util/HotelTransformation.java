package com.booking.util;

import com.booking.business.dto.HotelDTO;
import com.booking.dataAccess.model.Hotel;

public class HotelTransformation {

    private HotelTransformation() {

    }

    public static HotelDTO toDto (Hotel hotel){
        HotelDTO hotelDTO = new HotelDTO();
        hotelDTO.setCity(hotel.getCity());
        hotelDTO.setCountry(hotel.getCountry());
        hotelDTO.setIdHotel(hotel.getIdHotel());
        hotelDTO.setImage(hotel.getImage());
        hotelDTO.setInformation(hotel.getInformation());
        hotelDTO.setName(hotel.getName());
        hotelDTO.setNumberStars(hotel.getNumberStars());
        hotelDTO.setNumberDoubleRooms(hotel.getNumberDoubleRooms());
        hotelDTO.setNumberTripleRooms(hotel.getNumberTripleRooms());
        return hotelDTO;
    }

    public static Hotel toHotel(HotelDTO hotelDTO){
        Hotel hotel = new Hotel();
        hotel.setCity(hotelDTO.getCity());
        hotel.setCountry(hotelDTO.getCountry());
        hotel.setIdHotel(hotelDTO.getIdHotel());
        hotel.setImage(hotelDTO.getImage());
        hotel.setInformation(hotelDTO.getInformation());
        hotel.setName(hotelDTO.getName());
        hotel.setNumberStars(hotelDTO.getNumberStars());
        hotel.setNumberTripleRooms(hotelDTO.getNumberTripleRooms());
        hotel.setNumberDoubleRooms(hotelDTO.getNumberDoubleRooms());
        return hotel;
    }
}
