package com.booking.util;

import com.booking.business.dto.OfferDTO;
import com.booking.dataAccess.model.Offer;

public class OfferTransformation {

    private OfferTransformation(){

    }

    public static OfferDTO toDto(Offer offer){
        OfferDTO offerDTO = new OfferDTO();
        offerDTO.setHotel(offer.getHotel());
        offerDTO.setDataEnd(offer.getDataEnd());
        offerDTO.setDateStart(offer.getDateStart());
        offerDTO.setIdOffer(offer.getIdOffer());
        offerDTO.setPrice(offer.getPrice());
        offerDTO.setNumberDoubleRooms(offer.getNumberDoubleRooms());
        offerDTO.setNumberTripleRooms(offer.getNumberTripleRooms());
        return offerDTO;
    }

    public static Offer toOffer(OfferDTO offerDTO){
        Offer offer = new Offer();
        offer.setDataEnd(offerDTO.getDataEnd());
        offer.setDateStart(offerDTO.getDateStart());
        offer.setHotel(offerDTO.getHotel());
        offer.setIdOffer(offerDTO.getIdOffer());
        offer.setNumberDoubleRooms(offerDTO.getNumberDoubleRooms());
        offer.setNumberTripleRooms(offerDTO.getNumberTripleRooms());
        offer.setPrice(offerDTO.getPrice());
        return offer;
    }
}
