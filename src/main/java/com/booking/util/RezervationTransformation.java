package com.booking.util;

import com.booking.business.dto.RezervationDTO;
import com.booking.dataAccess.model.Rezervation;

public class RezervationTransformation {

    public RezervationTransformation(){

    }

    public static RezervationDTO toDto(Rezervation rezervation){
        RezervationDTO rezervationDTO = new RezervationDTO();
        rezervationDTO.setIdRezervation(rezervation.getIdRezervation());
        rezervationDTO.setOffer(rezervation.getOffer());
        rezervationDTO.setUser(rezervation.getUser());
        rezervationDTO.setNumberTripleRoom(rezervation.getNumberTripleRoom());
        rezervationDTO.setNumerDoubleRoom(rezervation.getNumberDoubleRoom());
        return rezervationDTO;
    }

    public static Rezervation toRezervation(RezervationDTO rezervationDTO){
        Rezervation rezervation = new Rezervation();
        rezervation.setIdRezervation(rezervationDTO.getIdRezervation());
        rezervation.setOffer(rezervationDTO.getOffers());
        rezervation.setUser(rezervationDTO.getUser());
        rezervation.setNumberDoubleRoom(rezervationDTO.getNumberDoubleRoom());
        rezervation.setNumberTripleRoom(rezervationDTO.getNumberTripleRoom());
        return rezervation;
    }


}
