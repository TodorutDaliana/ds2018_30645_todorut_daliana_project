package com.booking.util;

import com.booking.business.dto.UserDTO;
import com.booking.dataAccess.model.User;

public class UserTransformation {

    private UserTransformation() {

    }

    public static UserDTO toDto (User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setTelephoneNumber(user.getTelephoneNumber());
        userDTO.setTypeUser(user.getTypeUser());
        userDTO.setLastname(user.getLastname());
        userDTO.setActive(user.isActive());
        return userDTO;
    }

    public static User toUser(UserDTO userDTO){
        User user = new User();
        user.setId(userDTO.getId());
        user.setFirstname(userDTO.getFirstname());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setTelephoneNumber(userDTO.getTelephoneNumber());
        user.setTypeUser(userDTO.getTypeUser());
        user.setLastname(userDTO.getLastname());
        user.setActive(userDTO.isActive());
        return user;

    }

}
